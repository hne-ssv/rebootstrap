#!/bin/bash
#
# Start a (re-)build for all cross-essensial debian packages i586
#
# Please copy this file before edit, to have settings for your self!
# Options, you not should change:
#   mmdebstrap --mode=root
#   ENABLE_MULTILIB=no
#   ENABLE_MULTIARCH_GCC=yes
#   HOST_ARCH=i586

# All exported environment variables here are visible in customize-hook and
# in bootstrap.sh as defaults.
# This is more easy, as multiple escaping between shell args.

# Mirror of Debian repository
MIRROR=http://ftp.de.debian.org/debian

# where to run the build
CHROOT_DIR="../debian-bookworm-i586-chroot-MUAR"

# Fix problem with DROP_PRIVS=buildd and "mmdebstrap --mode=root ...",
# and using ccache.
# Some cache directories are created before using runuser.
# These as root created directories can runuser not write.
export CCACHE_UMASK=0002

# A directory where built packages should be placed
# Same directory inside chroot, mounted via bind
export REPODIR="/workdir/MUAR/repo"

# added to exclude list: objcxx fortran
# Comma must use here to push it into bootstrap.sh as argument key=val
NOLANG="ada,asan,brig,d,gcn,go,itm,java,jit,hppa64,lsan,m2,nvptx,objc,obj-c++,rust,tsan,ubsan,objcxx,fortran"

# This script can run only ones
PIDFILE=/run/rebootstrap-muar.pid
if [ -f $PIDFILE ]; then
	PID=`cat $PIDFILE`
	if [ -L /proc/$PID/exe ]; then
		echo "rebootstrap is running as $PID"
		exit 5
	fi
fi
echo "$$" >$PIDFILE

# Manage restart for next stamps
restart_build()
{
    set -e
    if [ ! -d "$1/proc/1" ]; then
        if ! mount -t proc proc "$1/proc"; then
            echo "Can not mount /proc inside chroot" >&2
            echo "Maybe start container with '--privileged' ?" >&2
            exit 1
        fi
    fi
    d="$REPODIR"; mkdir -p "$d" "$1$d"; test -d "$1/$d/pool" || mount --bind "$d" "$1$d"
    set +e

    chroot "$1" bash /bootstrap.sh HOST_ARCH=i586 \
      REPODIR="$REPODIR" CODENAME="bookworm" \
      PATH="/usr/lib/ccache:\$PATH" GCC_NOLANG="$NOLANG"
    RC=$?

    echo "Total deb-Packages:`find $1$REPODIR/pool -type f -name '*.deb' | wc -l`"
    umount "$1$REPODIR" 2>/dev/null || true
    umount "$1/proc" 2>/dev/null || true
    return $RC
}

# Start fresh build
start_build()
{
    mmdebstrap --verbose --variant=apt --mode=root \
      --skip=cleanup/reproducible \
      --dpkgopt='path-exclude=/usr/share/man/*' \
      --dpkgopt='path-include=/usr/share/man/man[1-9]/*' \
      --dpkgopt='path-exclude=/usr/share/locale/*' \
      --include="ccache" \
      --setup-hook='d="$REPODIR"; mkdir -p "$d" "$1$d"; mount --bind "$d" "$1$d"' \
      --customize-hook="upload bootstrap.sh /bootstrap.sh" \
      --customize-hook='chroot "$1" ccache --max-size 4G' \
      --customize-hook='chroot "$1" update-ccache-symlinks' \
      --customize-hook='chroot "$1" bash /bootstrap.sh HOST_ARCH=i586 \
          REPODIR="$REPODIR" CODENAME=bookworm \
          PATH="/usr/lib/ccache:\$PATH" GCC_NOLANG="'$NOLANG'"; \
          RC=$?; umount "$1$REPODIR"; exit $RC;' \
      bookworm $CHROOT_DIR $MIRROR
}

if [ -d "$CHROOT_DIR/tmp" ]; then
    restart_build "$CHROOT_DIR"
else
    start_build "$CHROOT_DIR"
fi

# for mode=root: umount in case of fail out from mmdebstrap
# Umount Repo
if [ -n "$REPODIR" -a -d "$CHROOT_DIR$REPODIR/pool" ]; then
    umount "$CHROOT_DIR$REPODIR" || true
fi

rm -f $PIDFILE

# Links:
# https://stackoverflow.com/questions/46480966/how-to-rebuild-all-debian-packages-of-a-system-with-specific-flag
# https://wiki.debian.org/HelmutGrohne/rebootstrap
# https://manpages.debian.org/testing/mmdebstrap/mmdebstrap.1.en.html
# https://wiki.ubuntu.com/MultiarchCross

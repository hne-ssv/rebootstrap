#!/bin/bash

# https://wiki.debian.org/HelmutGrohne/rebootstrap

set -v
set -e
set -u

# Print Line Number for every debug output. Bash only.
export PS4='bootstrap.sh $LINENO: '

export DEB_BUILD_OPTIONS="nocheck noddebs parallel=1"
export DH_VERBOSE=1
HOST_ARCH=undefined
# select gcc version from gcc-defaults package unless set
GCC_VER=
: ${MIRROR:="http://http.debian.net/debian"}
ENABLE_MULTILIB=no
ENABLE_MULTIARCH_GCC=yes
REPODIR=/tmp/repo
APT_GET="apt-get --no-install-recommends -y -o Debug::pkgProblemResolver=true -o Debug::pkgDepCache::Marker=1 -o Debug::pkgDepCache::AutoInstall=1 -o Acquire::Languages=none"
DEFAULT_PROFILES="cross nocheck noinsttest noudeb"
DROP_PRIVS=buildd
GCC_NOLANG="ada asan brig d gcn go itm java jit hppa64 lsan m2 nvptx objc obj-c++ rust tsan ubsan"
ENABLE_DIFFOSCOPE=no
CODENAME=sid
MAINTAINER_EMAIL="invalid@invalid"
MAINTAINER_NAME="rebootstrap"
BUILD_TYPE=any

if df -t tmpfs /var/cache/apt/archives >/dev/null 2>&1; then
	APT_GET="$APT_GET -o APT::Keep-Downloaded-Packages=false"
fi

if test "$(hostname -f)" = ionos9-amd64.debian.net; then
	# jenkin's proxy fails very often
	echo 'APT::Acquire::Retries "10";' > /etc/apt/apt.conf.d/80-retries
fi

# evaluate command line parameters of the form KEY=VALUE
for param in "$@"; do
	echo "bootstrap-configuration: $param"
	eval $param
done

# test whether element $2 is in set $1
set_contains() {
	case " $1 " in
		*" $2 "*) return 0; ;;
		*) return 1; ;;
	esac
}

# add element $2 to set $1
set_add() {
	case " $1 " in
		"  ") echo "$2" ;;
		*" $2 "*) echo "$1" ;;
		*) echo "$1 $2" ;;
	esac
}

# remove element $2 from set $1
set_discard() {
	local word result
	if set_contains "$1" "$2"; then
		result=
		for word in $1; do
			test "$word" = "$2" || result="$result $word"
		done
		echo "${result# }"
	else
		echo "$1"
	fi
}

# create a set from a string of words with duplicates and excess white space
set_create() {
	local word result
	result=
	for word in $1; do
		result=`set_add "$result" "$word"`
	done
	echo "$result"
}

# intersect two sets
set_intersect() {
	local word result
	result=
	for word in $1; do
		if set_contains "$2" "$word"; then
			result=`set_add "$result" "$word"`
		fi
	done
	echo "$result"
}

# compute the set of elements in set $1 but not in set $2
set_difference() {
	local word result
	result=
	for word in $1; do
		if ! set_contains "$2" "$word"; then
			result=`set_add "$result" "$word"`
		fi
	done
	echo "$result"
}

# compute the union of two sets $1 and $2
set_union() {
	local word result
	result=$1
	for word in $2; do
		result=`set_add "$result" "$word"`
	done
	echo "$result"
}

# join the words the arguments starting with $2 with separator $1
join_words() {
	local separator word result
	separator=$1
	shift
	result=
	for word in "$@"; do
		result="${result:+$result$separator}$word"
	done
	echo "$result"
}

check_arch() {
	if elf-arch -a "$2" "$1"; then
		return 0
	else
		case "$2:$(file -b "$1")" in
			"arc:ELF 32-bit LSB relocatable, *unknown arch 0xc3* version 1 (SYSV)"*|"arc:ELF 32-bit LSB relocatable, Synopsys ARCv2/HS3x/HS4x cores, version 1 (SYSV)"*)
				return 0
			;;
			"csky:ELF 32-bit LSB relocatable, *unknown arch 0xfc* version 1 (SYSV)"*|"csky:ELF 32-bit LSB relocatable, C-SKY processor family, version 1 (SYSV)"*)
				return 0
			;;
			"loong64:ELF 64-bit LSB relocatable, LoongArch, version 1 (SYSV)"*)
				return 0
			;;
		esac
		echo "expected $2, but found $(file -b "$1")"
		return 1
	fi
}

filter_dpkg_tracked() {
	local pkg pkgs
	pkgs=""
	for pkg in "$@"; do
		dpkg-query -s "$pkg" >/dev/null 2>&1 && pkgs=`set_add "$pkgs" "$pkg"`
	done
	echo "$pkgs"
}

apt_get_install() {
	DEBIAN_FRONTEND=noninteractive $APT_GET install "$@"
}

apt_get_build_dep() {
	DEBIAN_FRONTEND=noninteractive $APT_GET build-dep "$@"
}

apt_get_remove() {
	local pkgs
	pkgs=$(filter_dpkg_tracked "$@")
	if test -n "$pkgs"; then
		$APT_GET remove $pkgs
	fi
}

apt_get_purge() {
	local pkgs
	pkgs=$(filter_dpkg_tracked "$@")
	if test -n "$pkgs"; then
		$APT_GET purge $pkgs
	fi
}

$APT_GET update
$APT_GET dist-upgrade # we need upgrade later, so make sure the system is clean
apt_get_install build-essential debhelper reprepro quilt arch-test python3

# Preinstall for mostly docs
# Build-Depends-Indep: gtk-doc-tools, texinfo, texlive-latex-base
if [ "$BUILD_TYPE" = "any,all" ]; then
	apt_get_install gtk-doc-tools texinfo texlive-latex-base  help2man
fi

if test -z "$DROP_PRIVS"; then
	drop_privs_exec() {
		exec env -- "$@"
	}
else
	# Multiarch=yes needs fakeroot
	apt_get_install adduser fakeroot
	if ! getent passwd "$DROP_PRIVS" >/dev/null; then
		adduser --system --group --home /tmp/buildd --no-create-home --shell /bin/false "$DROP_PRIVS"
	fi
	drop_privs_exec() {
		exec /sbin/runuser --user "$DROP_PRIVS" --group "$DROP_PRIVS" -- /usr/bin/env -- "$@"
	}
fi
drop_privs() {
	( drop_privs_exec "$@" )
}

if test -z "$GCC_VER"; then
	GCC_VER=`apt-cache depends gcc | sed 's/^ *Depends: gcc-\([0-9.]*\)$/\1/;t;d'`
fi

if test "$ENABLE_MULTIARCH_GCC" = yes; then
	apt_get_install cross-gcc-dev
	echo "removing unused unstripped_exe patch"
	sed -i '/made-unstripped_exe-setting-overridable/d' /usr/share/cross-gcc/patches/gcc-*/series
fi

obtain_source_package() {
	local use_experimental
	use_experimental=
	case "$1" in
		gcc-[0-9]*)
			test -n "$(apt-cache showsrc "$1")" || use_experimental=yes
		;;
	esac
	if test "$use_experimental" = yes; then
		echo "deb-src $MIRROR experimental main" > /etc/apt/sources.list.d/tmp-experimental.list
		$APT_GET update
	fi
	drop_privs apt-get source "$1"
	if test -f /etc/apt/sources.list.d/tmp-experimental.list; then
		rm /etc/apt/sources.list.d/tmp-experimental.list
		$APT_GET update
	fi
}

# i586: Added for Vortex
grep -q 'i586' /usr/share/dpkg/cputable || cat <<EOF >> /usr/share/dpkg/cputable
i586		i586		(i586|vortex)	32	little
csky		csky		csky		32	little
EOF
# Remove i586 from config.guess
sed 's/34567/3467/' -i /usr/share/dpkg/cputable

if test -z "$HOST_ARCH" || ! dpkg-architecture "-a$HOST_ARCH"; then
	echo "architecture $HOST_ARCH unknown to dpkg"
	exit 1
fi

if test "$HOST_ARCH" = "i586"; then
	# i586 not as multilib
	echo "i586 is not a multilib of i686"
	sed -i -e 's/i\[4567\]86/i\[467\]86/' /usr/share/perl5/Dpkg/Arch.pm
	# Pitfall: Check it
	T=`dpkg-architecture -ai586 -qDEB_TARGET_MULTIARCH`
	if test "$T" != "i586-linux-gnu"; then
		echo "architecture $HOST_ARCH wrong DEB_TARGET_MULTIARCH $T, should i586-linux-gnu"
		exit 1
	fi

	# i586 builds elf code for i386, make `elf-arch -ai586 test.o` happy
	grep -q 'i586' /usr/bin/elf-arch || patch -p1 <<'EOF'
--- a/usr/bin/elf-arch
+++ b/usr/bin/elf-arch
@@ -132,6 +132,7 @@
 $a=~s/^linux-//;
 
 my %aliases=(
+    'i386' => ['i586'],
     'arm' => ['armel', 'armhf'],
     'powerpc' => ['powerpcspe'],
     'sh4' => ['sh3'],
EOF
fi

# ensure that the rebootstrap list comes first
test -f /etc/apt/sources.list && mv -v /etc/apt/sources.list /etc/apt/sources.list.d/local.list
grep -q "^deb-src .* $CODENAME" /etc/apt/sources.list.d/*.list || echo "deb-src $MIRROR $CODENAME main" >> /etc/apt/sources.list.d/$CODENAME-source.list

dpkg --add-architecture $HOST_ARCH
$APT_GET update

rm -Rf /tmp/buildd
drop_privs mkdir -p /tmp/buildd

HOST_ARCH_SUFFIX="-`dpkg-architecture -a$HOST_ARCH -qDEB_HOST_GNU_TYPE | tr _ -`"

case "$HOST_ARCH" in
	amd64) MULTILIB_NAMES="i386 x32" ;;
	i386) MULTILIB_NAMES="amd64 x32" ;;
	mips|mipsel) MULTILIB_NAMES="mips64 mipsn32" ;;
	mips64|mips64el) MULTILIB_NAMES="mips32 mipsn32" ;;
	mipsn32|mipsn32el) MULTILIB_NAMES="mips32 mips64" ;;
	powerpc) MULTILIB_NAMES=ppc64 ;;
	ppc64) MULTILIB_NAMES=powerpc ;;
	s390x) MULTILIB_NAMES=s390 ;;
	sparc) MULTILIB_NAMES=sparc64 ;;
	sparc64) MULTILIB_NAMES=sparc ;;
	x32) MULTILIB_NAMES="amd64 i386" ;;
	*) MULTILIB_NAMES="" ;;
esac
if test "$ENABLE_MULTILIB" != yes; then
	MULTILIB_NAMES=""
fi

for f in /etc/apt/sources.list.d/*.list; do
	test -f "$f" && sed -i "s/^deb \(\[.*\] \)*/deb [ arch-=$HOST_ARCH ] /" "$f"
done
mkdir -p "$REPODIR/conf" "$REPODIR/archive" "$REPODIR/stamps"
cat > "$REPODIR/conf/distributions" <<EOF
Codename: rebootstrap
Label: rebootstrap
Architectures: `dpkg --print-architecture` $HOST_ARCH
Components: main
UDebComponents: main
Description: cross toolchain and build results for $HOST_ARCH

Codename: rebootstrap-native
Label: rebootstrap-native
Architectures: `dpkg --print-architecture`
Components: main
UDebComponents: main
Description: native packages needed for bootstrap
EOF
cat > "$REPODIR/conf/options" <<EOF
verbose
ignore wrongdistribution
EOF
export REPREPRO_BASE_DIR="$REPODIR"
reprepro export
echo "deb [ arch=$(dpkg --print-architecture),$HOST_ARCH trusted=yes ] file://$REPODIR rebootstrap main" >/etc/apt/sources.list.d/000_rebootstrap.list
echo "deb [ arch=$(dpkg --print-architecture) trusted=yes ] file://$REPODIR rebootstrap-native main" >/etc/apt/sources.list.d/001_rebootstrap-native.list
cat >/etc/apt/preferences.d/rebootstrap.pref <<EOF
Explanation: prefer our own rebootstrap (native) packages over everything
Package: *
Pin: release l=rebootstrap-native
Pin-Priority: 1001

Explanation: prefer our own rebootstrap (toolchain) packages over everything
Package: *
Pin: release l=rebootstrap
Pin-Priority: 1002

Explanation: do not use archive cross toolchain
Package: *-$HOST_ARCH-cross *$HOST_ARCH_SUFFIX gcc-*$HOST_ARCH_SUFFIX-base
Pin: release a=$CODENAME
Pin-Priority: -1
EOF
$APT_GET update

# Since most libraries (e.g. libgcc_s) do not include ABI-tags,
# glibc may be confused and try to use them. A typical symptom is:
# apt-get: error while loading shared libraries: /lib/x86_64-kfreebsd-gnu/libgcc_s.so.1: ELF file OS ABI invalid
cat >/etc/dpkg/dpkg.cfg.d/ignore-foreign-linker-paths <<EOF
path-exclude=/etc/ld.so.conf.d/$(dpkg-architecture "-a$HOST_ARCH" -qDEB_HOST_MULTIARCH).conf
EOF

# Work around Multi-Arch: same file conflict in libxdmcp-dev. #825146
cat >/etc/dpkg/dpkg.cfg.d/bug-825146 <<'EOF'
path-exclude=/usr/share/doc/libxdmcp-dev/xdmcp.txt.gz
EOF

## Work around Multi-Arch: same file conflict in nghttp2, libxaw, pam
cat >/etc/dpkg/dpkg.cfg.d/bug-nghttp2 <<'EOF'
path-exclude=/usr/share/man/man3/ev.3.gz
EOF

cat >/etc/dpkg/dpkg.cfg.d/bug-binutils-multiarch <<'EOF'
path-exclude=/usr/bin/curl-config
EOF

cat >/etc/dpkg/dpkg.cfg.d/bug-libxaw-all <<'EOF'
path-exclude=/usr/share/man/man3/XauDisposeAuth.3.gz
EOF

cat >/etc/dpkg/dpkg.cfg.d/bug-pam-all <<'EOF'
path-exclude=/usr/share/man/man3/crypt_gensalt_ra.3.gz
EOF

cat >/etc/dpkg/dpkg.cfg.d/bug-libxext-dev <<'EOF'
path-exclude=/usr/share/man/man3/XShapeCombineMask.3.gz
EOF

# Work around binNMU file conflicts of e.g. binutils or gcc.
cat >/etc/dpkg/dpkg.cfg.d/binNMU-changelogs <<EOF
path-exclude=/usr/share/doc/*/changelog.Debian.$(dpkg-architecture -qDEB_BUILD_ARCH).gz
EOF

# debhelper/13.10 started trimming changelogs, this breaks all over the place
cat >/etc/dpkg/dpkg.cfg.d/trimmed-changelogs <<'EOF'
path-exclude=/usr/share/doc/*/changelog.Debian.gz
path-exclude=/usr/share/doc/*/changelog.gz
EOF

if test "$HOST_ARCH" = nios2; then
	echo "fixing libtool's nios2 misdetection as os2 #851253"
	apt_get_install libtool
	sed -i -e 's/\*os2\*/*-os2*/' /usr/share/libtool/build-aux/ltmain.sh
fi

# removing libc*-dev conflict with each other
LIBC_DEV_PKG=$(apt-cache showpkg libc-dev | sed '1,/^Reverse Provides:/d;s/ .*//;q')
if test "$(apt-cache show "$LIBC_DEV_PKG" | sed -n 's/^Source: //;T;p;q')" = glibc; then
	if test -f "$REPODIR/pool/main/g/glibc/$LIBC_DEV_PKG"_*_"$(dpkg --print-architecture).deb"; then
		dpkg -i "$REPODIR/pool/main/g/glibc/$LIBC_DEV_PKG"_*_"$(dpkg --print-architecture).deb"
	else
		cd /tmp/buildd
		apt-get download "$LIBC_DEV_PKG"
		dpkg-deb -R "./$LIBC_DEV_PKG"_*.deb x
		sed -i -e '/^Conflicts: /d' x/DEBIAN/control
		mv -nv -t x/usr/include "x/usr/include/$(dpkg-architecture -qDEB_HOST_MULTIARCH)/"*
		mv -nv x/usr/include x/usr/include.orig
		mkdir x/usr/include
		mv -nv x/usr/include.orig "x/usr/include/$(dpkg-architecture -qDEB_HOST_MULTIARCH)"
		dpkg-deb -b x "./$LIBC_DEV_PKG"_*.deb
		reprepro includedeb rebootstrap-native "./$LIBC_DEV_PKG"_*.deb
		dpkg -i "./$LIBC_DEV_PKG"_*.deb
		$APT_GET update
		rm -R "./$LIBC_DEV_PKG"_*.deb x
	fi # already repacked
fi # is glibc

chdist_native() {
	local command
	command="$1"
	shift
	chdist --data-dir /tmp/chdist_native --arch "$HOST_ARCH" "$command" native "$@"
}

if test "$ENABLE_DIFFOSCOPE" = yes; then
	apt_get_install devscripts
	chdist_native create "$MIRROR" "$CODENAME" main
	if ! chdist_native apt-get update; then
		echo "rebootstrap-warning: not comparing packages to native builds"
		rm -Rf /tmp/chdist_native
		ENABLE_DIFFOSCOPE=no
	fi
fi
if test "$ENABLE_DIFFOSCOPE" = yes; then
	compare_native() {
		local pkg pkgname tmpdir downloadname errcode
		apt_get_install diffoscope binutils-multiarch vim-common
		for pkg in "$@"; do
			if test "`dpkg-deb -f "$pkg" Architecture`" != "$HOST_ARCH"; then
				echo "not comparing $pkg: wrong architecture"
				continue
			fi
			pkgname=`dpkg-deb -f "$pkg" Package`
			tmpdir=`mktemp -d`
			mkdir "$tmpdir/a" "$tmpdir/b"
			cp "$pkg" "$tmpdir/a" # work around diffoscope recursing over the build tree
			if ! (cd "$tmpdir/b" && chdist_native apt-get download "$pkgname"); then
				echo "not comparing $pkg: download failed"
				rm -R "$tmpdir"
				continue
			fi
			downloadname=`dpkg-deb -W --showformat '${Package}_${Version}_${Architecture}.deb' "$pkg" | sed 's/:/%3a/'`
			if ! test -f "$tmpdir/b/$downloadname"; then
				echo "not comparing $pkg: downloaded different version"
				rm -R "$tmpdir"
				continue
			fi
			errcode=0
			timeout --kill-after=1m 1h diffoscope --text "$tmpdir/out" "$tmpdir/a/$(basename -- "$pkg")" "$tmpdir/b/$downloadname" || errcode=$?
			case $errcode in
				0)
					echo "diffoscope-success: $pkg"
				;;
				1)
					if ! test -f "$tmpdir/out"; then
						echo "rebootstrap-error: no diffoscope output for $pkg"
						exit 1
					elif test "`wc -l < "$tmpdir/out"`" -gt 1000; then
						echo "truncated diffoscope output for $pkg:"
						head -n1000 "$tmpdir/out"
					else
						echo "diffoscope output for $pkg:"
						cat "$tmpdir/out"
					fi
				;;
				124)
					echo "rebootstrap-warning: diffoscope timed out"
				;;
				*)
					echo "rebootstrap-error: diffoscope terminated with abnormal exit code $errcode"
					exit 1
				;;
			esac
			rm -R "$tmpdir"
		done
	}
else
	compare_native() { :
	}
fi

pickup_additional_packages() {
	local f
	for f in "$@"; do
		if test "${f%.deb}" != "$f"; then
			reprepro includedeb rebootstrap "$f"
		elif test "${f%.changes}" != "$f"; then
			reprepro include rebootstrap "$f"
		else
			echo "cannot pick up package $f"
			exit 1
		fi
	done
	$APT_GET update
}

pickup_packages() {
	local sources
	local source
	local f
	local i
	# collect source package names referenced
	sources=""
	for f in "$@"; do
		if test "${f%.deb}" != "$f"; then
			source=`dpkg-deb -f "$f" Source`
			test -z "$source" && source=${f%%_*}
		elif test "${f%.changes}" != "$f"; then
			source=${f%%_*}
		else
			echo "cannot pick up package $f"
			exit 1
		fi
		sources=`set_add "$sources" "$source"`
	done
	# archive old contents and remove them from the repository
	for source in $sources; do
		i=1
		while test -e "$REPODIR/archive/${source}_$i"; do
			i=`expr $i + 1`
		done
		i="$REPODIR/archive/${source}_$i"
		mkdir "$i"
		for f in $(reprepro --list-format '${Filename}\n' listfilter rebootstrap "\$Source (== $source)"); do
			cp -v "$REPODIR/$f" "$i"
		done
		find "$i" -type d -empty -delete
		reprepro removesrc rebootstrap "$source"
	done
	# add new contents
	pickup_additional_packages "$@"
}

# compute a function name from a hook prefix $1 and a package name $2
# returns success if the function actually exists
get_hook() {
	local hook
	hook=`echo "$2" | tr -- -. __` # - and . are invalid in function names
	hook="${1}_$hook"
	echo "$hook"
	type "$hook" >/dev/null 2>&1 || return 1
}

cross_build_setup() {
	local pkg subdir hook
	pkg="$1"
	subdir="${2:-$pkg}"
	cd /tmp/buildd
	drop_privs mkdir "$subdir"
	cd "$subdir"
	obtain_source_package "$pkg"
	cd "${pkg}-"*
	hook=`get_hook patch "$pkg"` && "$hook"
	return 0
}

# add a binNMU changelog entry
# . is a debian package
# $1 is the binNMU number
# $2 is reason
add_binNMU_changelog() {
	cat - debian/changelog <<EOF |
$(dpkg-parsechangelog -SSource) ($(dpkg-parsechangelog -SVersion)+b$1) $CODENAME; urgency=medium, binary-only=yes

  * Binary-only non-maintainer upload for $HOST_ARCH; no source changes.
  * $2

 -- $MAINTAINER_NAME <$MAINTAINER_EMAIL>  $(dpkg-parsechangelog -SDate)

EOF
	drop_privs tee debian/changelog.new >/dev/null
	drop_privs mv debian/changelog.new debian/changelog
}

check_binNMU() {
	local pkg srcversion binversion maxversion
	srcversion=`dpkg-parsechangelog -SVersion`
	maxversion=$srcversion
	for pkg in `dh_listpackages`; do
		binversion=`apt-cache show "$pkg=$srcversion*" 2>/dev/null | sed -n 's/^Version: //p;T;q'`
		test -z "$binversion" && continue
		if dpkg --compare-versions "$maxversion" lt "$binversion"; then
			maxversion=$binversion
		fi
	done
	case "$maxversion" in
		"$srcversion+b"*)
			echo "rebootstrap-warning: binNMU detected for $(dpkg-parsechangelog -SSource) $srcversion/$maxversion"
			add_binNMU_changelog "${maxversion#$srcversion+b}" "Bump to binNMU version of $(dpkg --print-architecture)."
		;;
	esac
}

PROGRESS_MARK=1
progress_mark() {
	echo "progress-mark:$PROGRESS_MARK:$*" | tee -a $REPODIR/progress_marks.log
	PROGRESS_MARK=$(($PROGRESS_MARK + 1 ))
}

# prints the set (as in set_create) of installed packages
record_installed_packages() {
	dpkg --get-selections | sed 's/\s\+install$//;t;d' | xargs
}

# Takes the set (as in set_create) of packages and apt-get removes any
# currently installed packages outside the given set.
remove_extra_packages() {
	local origpackages currentpackages removedpackages extrapackages
	origpackages="$1"
	currentpackages=$(record_installed_packages)
	removedpackages=$(set_difference "$origpackages" "$currentpackages")
	extrapackages=$(set_difference "$currentpackages" "$origpackages")
	echo "original packages: $origpackages"
	echo "removed packages:  $removedpackages"
	echo "extra packages:    $extrapackages"
	apt_get_remove $extrapackages
}

buildpackage_failed() {
	local err last_config_log
	err="$1"
	echo "rebootstrap-error: dpkg-buildpackage failed with status $err"
	last_config_log=$(find . -type f -name config.log -printf "%T@ %p\n" | sort -g | tail -n1 | cut "-d " -f2-)
	if test -f "$last_config_log"; then
		tail -v -n+0 "$last_config_log"
	fi
	exit "$err"
}

# $1:Source package name, $2:additional profiles $3:stamp $4:buildtype
cross_build() {
	local pkg profiles buildtype stamp ignorebd hook installedpackages
	pkg="$1"
	profiles="$DEFAULT_PROFILES ${2:-}"
	stamp="${3:-$pkg}"
	buildtype="${4:-$BUILD_TYPE}"
	if test "$ENABLE_MULTILIB" = "no"; then
		profiles="$profiles nobiarch"
	fi
	profiles=$(join_words , $profiles)
	if test -f "$REPODIR/stamps/$stamp"; then
		echo "skipping rebuild of $pkg with profiles $profiles"
	else
		echo "building $pkg with profiles $profiles"
		cross_build_setup "$pkg" "$stamp"
		installedpackages=$(record_installed_packages)
		ignorebd=
		if hook=`get_hook builddep "$pkg"`; then
			echo "installing Build-Depends for $pkg using custom function"
			"$hook" "$HOST_ARCH" "$profiles"
		else
			echo "installing Build-Depends for $pkg using apt-get build-dep"
			apt_get_build_dep "-a$HOST_ARCH" --arch-only "-P$profiles" ./
			if [ "$BUILD_TYPE" = "any,all" ]; then
				if ! dpkg-checkbuilddeps -A "-a$HOST_ARCH" "-P$profiles"; then
					echo "installing Build-Depends-Indep for $pkg using apt-get build-dep"
					apt_get_build_dep "-P$profiles" ./
					ignorebd=-d
				fi
			fi
		fi
		check_binNMU
		if get_hook builddep "$pkg" >/dev/null; then
			if dpkg-checkbuilddeps -B "-a$HOST_ARCH" "-P$profiles"; then
				echo "rebootstrap-warning: Build-Depends for $pkg satisfied even though a custom builddep_  function is in use"
			fi
			ignorebd=-d
		fi
		(
			if hook=`get_hook buildenv "$pkg"`; then
				echo "adding environment variables via buildenv hook for $pkg"
				"$hook" "$HOST_ARCH"
			fi
			# Auto fix for ENABLE_MULTIARCH_GCC=no
			if test "$ENABLE_MULTIARCH_GCC" != yes; then
				if grep -q 'usr/include/$(DEB_HOST_MULTIARCH)' debian/rules && \
				 ! grep -q 'usr/$(DEB_HOST_MULTIARCH)/include' debian/rules; then
					echo "auto fix include Multiarch for $pkg"
					mkdir -p "$REPODIR/rules-fix"
					cp debian/rules "$REPODIR/rules-fix/$pkg.rules"
					drop_privs sed -e 's|usr/include/$(DEB_HOST_MULTIARCH)|usr/$(DEB_HOST_MULTIARCH)/include|g' \
						       -i.vortex debian/rules
					cp debian/rules "$REPODIR/rules-fix/$pkg.rules.fixed"
				fi
			fi
			drop_privs_exec dpkg-buildpackage "-a$HOST_ARCH" "--build=$buildtype" "-P$profiles" $ignorebd -uc -us
		) || buildpackage_failed "$?"
		cd ..
		remove_extra_packages "$installedpackages"
		ls -l
		pickup_packages *.changes
		touch "$REPODIR/stamps/$stamp"
		compare_native ./*.deb
		cd ..
		drop_privs rm -Rf "$stamp"
	fi
	progress_mark "$stamp cross build"
}

if test "$ENABLE_MULTIARCH_GCC" != yes; then
	echo "install dpkg-cross for MULTIARCH_GCC=$ENABLE_MULTIARCH_GCC"
	apt_get_install dpkg-cross
fi

automatic_packages=
add_automatic() { automatic_packages=$(set_add "$automatic_packages" "$1"); }

add_automatic acl
add_automatic apt
patch_apt() {
	# CMake should find_path iconv.h in <prefix>/include/<arch> #796545
	drop_privs patch -p1 <<'EOF'
--- a/debian/rules
+++ b/debian/rules
@@ -17,6 +17,12 @@
 	configure_test_flags =
 endif
 
+ifeq ($(ENABLE_MULTIARCH_GCC), yes)
+	configure_library_architecture =
+else
+	configure_library_architecture = -DCMAKE_LIBRARY_ARCHITECTURE=$(DEB_HOST_MULTIARCH)
+endif
+
 %:
 	dh $@ --buildsystem=cmake+ninja
 
@@ -58,4 +64,5 @@
 endif
 
 override_dh_auto_configure-arch override_dh_auto_configure-indep:
-	dh_auto_configure -- -DCMAKE_BUILD_RPATH_USE_ORIGIN=ON $(configure_doc_flags) $(configure_test_flags)
+	dh_auto_configure -- -DCMAKE_BUILD_RPATH_USE_ORIGIN=ON $(configure_doc_flags) $(configure_test_flags) \
+				$(configure_library_architecture)
# Fix this
# i586-linux-gnu-g++: error: unrecognized command-line option '-n32'; did you mean '-m32'?
# ninja: build stopped: subcommand failed.
--- apt-2.6.1/CMake/FindLFS.cmake
+++ apt-2.6.1/CMake/FindLFS.cmake
@@ -125,9 +125,9 @@
                                    "${_lfs_libs_tmp};${_lfs_ldflags_tmp}")
     endif()
 
-    if(NOT LFS_FOUND)  # IRIX stuff
-        _lfs_check_compiler_option(lfs_need_n32 "-n32" "" "")
-    endif()
+#    if(NOT LFS_FOUND)  # IRIX stuff
+#        _lfs_check_compiler_option(lfs_need_n32 "-n32" "" "")
+#    endif()
     if(NOT LFS_FOUND)  # Linux and friends
         _lfs_check_compiler_option(lfs_need_file_offset_bits "" "-D_FILE_OFFSET_BITS=64" "")
     endif()
EOF
}
add_automatic attr
add_automatic base-files
add_automatic base-passwd
patch_base_passwd() {
	# fixing base-passwd: needs docbook #1033422
	apt_get_remove docbook-xml
}

add_automatic bash
patch_bash() {
	dpkg-architecture "-a$HOST_ARCH" -imusl-linux-any || return 0
	echo "fixing logic error around wrapping strtoimax #1023053"
	drop_privs patch -p1 <<'EOF'
--- a/configure
+++ b/configure
@@ -20443,7 +20443,7 @@

 { printf "%s\n" "$as_me:${as_lineno-$LINENO}: result: $bash_cv_func_strtoimax" >&5
 printf "%s\n" "$bash_cv_func_strtoimax" >&6; }
-if test $bash_cv_func_strtoimax = yes; then
+if test $bash_cv_func_strtoimax = no; then
 case " $LIBOBJS " in
   *" strtoimax.$ac_objext "* ) ;;
   *) LIBOBJS="$LIBOBJS strtoimax.$ac_objext"
EOF
}

patch_binutils() {
	echo "patching binutils to discard ldscripts"
	# They cause file conflicts with binutils and the in-archive cross
	# binutils discard ldscripts as well.
	drop_privs patch -p1 <<'EOF'
--- a/debian/rules
+++ b/debian/rules
@@ -751,6 +751,7 @@
 		mandir=$(pwd)/$(D_CROSS)/$(PF)/share/man install
 
 	rm -rf \
+		$(D_CROSS)/$(PF)/lib/ldscripts \
 		$(D_CROSS)/$(PF)/share/info \
 		$(D_CROSS)/$(PF)/share/locale
 
EOF
	if test "$HOST_ARCH" = hppa; then
		echo "patching binutils to discard hppa64 ldscripts"
		# They cause file conflicts with binutils and the in-archive
		# cross binutils discard ldscripts as well.
		drop_privs patch -p1 <<'EOF'
--- a/debian/rules
+++ b/debian/rules
@@ -1233,6 +1233,7 @@
 		$(d_hppa64)/$(PF)/lib/$(DEB_HOST_MULTIARCH)/.

 	: # Now get rid of just about everything in binutils-hppa64
+	rm -rf $(d_hppa64)/$(PF)/lib/ldscripts
 	rm -rf $(d_hppa64)/$(PF)/man
 	rm -rf $(d_hppa64)/$(PF)/info
 	rm -rf $(d_hppa64)/$(PF)/include
EOF
	fi
	echo "fix honouring of nocheck option #990794"
	drop_privs sed -i -e 's/ifeq (\(,$(filter $(DEB_HOST_ARCH),\)/ifneq ($(DEB_BUILD_ARCH)\1/' debian/rules
	case "$HOST_ARCH" in nios2|sparc)
		echo "enabling uncommon architectures in debian/control"
		drop_privs sed -i -e "/^#NATIVE_ARCHS +=/aNATIVE_ARCHS += $HOST_ARCH" debian/rules
		drop_privs ./debian/rules ./stamps/control
		drop_privs rm -f ./stamps/control
	;; esac
	echo "fix undefined symbol ldlex_defsym #992318"
	rm -f ld/ldlex.c

	if [ "$HOST_ARCH" = "i586" ]; then
		echo "Vortex: not map i586 to i686"
		drop_privs sed -i -e 's/subst i586,i686/subst i586_not_map,i686/' debian/rules
		drop_privs patch -p1 <<'EOF'
--- a/debian/rules
+++ b/debian/rules
@@ -35,7 +35,7 @@
 endif
 
 ifeq (,$(filter $(distrelease),lenny etch squeeze wheezy jessie))
-  ifeq ($(DEB_HOST_GNU_TYPE),i586-linux-gnu)
+  ifeq ($(DEB_HOST_GNU_TYPE),i586-linux-gnu_no_map)
     DEB_BUILD_GNU_TYPE	= i686-linux-gnu
     DEB_HOST_GNU_CPU	= i686
     DEB_HOST_GNU_TYPE	= i686-linux-gnu
@@ -888,6 +888,7 @@
 	rm -rf \
 		$(D_CROSS)/$(PF)/etc $(D_CROSS)/etc
 
+ifeq (,$(findstring stage2,$(DEB_BUILD_OPTIONS)))
 	mkdir -p $(D_CROSS)/$(PF)/lib/$(DEB_HOST_MULTIARCH)
 	set -e; \
 	d_src=$(D_CROSS)/$(PF)/$(DEB_HOST_GNU_TYPE)/$(CROSS_GNU_TYPE)/lib; \
@@ -907,6 +908,9 @@
 	  ln -sf ../../bin/$(CROSS_GNU_TYPE)-$$(basename $$i) $$i; \
 	done
 	ln -sf $(CROSS_GNU_TYPE)-ld.bfd $(D_CROSS)/usr/bin/$(CROSS_GNU_TYPE)-ld
+else
+	ln -sf ld.bfd $(D_CROSS)/usr/bin/$(CROSS_GNU_TYPE)-ld
+endif
 
 	$(call strip_package, $(P_CROSS),$(D_CROSS),.)
 	chmod ugo-x $(D_CROSS)/$(PF)/lib/$(DEB_HOST_MULTIARCH)/*.so
@@ -929,24 +933,32 @@
 	rm -f $(D_CROSS)/$(PF)/bin/*embedspu
 
 	: # no gprofng in cross packages
+	rm -f $(D_CROSS)/$(PF)/bin/gp-*
+	rm -f $(D_CROSS)/$(PF)/bin/gprofng
 	rm -f $(D_CROSS)/$(PF)/bin/*-gprofng
 	rm -f $(D_CROSS)/$(PF)/bin/*-gp-*
 	rm -rf $(D_CROSS)/$(PF)/lib/*/gprofng
+ifeq (,$(findstring stage2,$(DEB_BUILD_OPTIONS)))
 	rm -f $(D_CROSS)/$(PF)/lib/$(call _multiarch,$*)/libsframe*
+endif
 
 	: # Remove windows related manpages
 	rm -f $(D_CROSS)/$(PF)/share/man/man1/$(CROSS_GNU_TYPE)-{dlltool,nlmconv,windmc,windres}.1
 
 	: # symlink man pages
+ifeq (,$(findstring stage2,$(DEB_BUILD_OPTIONS)))
 	mv $(D_CROSS)/$(PF)/share/man/man1/$(CROSS_GNU_TYPE)-ld.1 \
 	  $(D_CROSS)/$(PF)/share/man/man1/$(CROSS_GNU_TYPE)-ld.bfd.1
+endif
 	gzip -9n $(D_CROSS)/$(PF)/share/man/man1/*
+ifeq (,$(findstring stage2,$(DEB_BUILD_OPTIONS)))
 	for i in $(D_CROSS)/$(PF)/share/man/man1/*; do \
 	  b=$$(basename $$i | sed 's/$(CROSS_GNU_TYPE)-//'); \
 	  ln -sf $$b $$i; \
 	done
 	ln -sf $(CROSS_GNU_TYPE)-ld.bfd.1.gz \
 	  $(D_CROSS)/$(PF)/share/man/man1/$(CROSS_GNU_TYPE)-ld.1.gz
+endif
 
 	: # gold man pages
 	$(if $(filter $*, $(gold_targets)), \
@@ -961,8 +973,10 @@
 	rm -f $(D_CROSS)/$(PF)/lib/*.la $(D_CROSS)/$(PF)/lib/*/*.la
 	rm -f $(D_CROSS)/$(PF)/$(DEB_HOST_GNU_TYPE)/$(CROSS_GNU_TYPE)/lib/*.a
 	rm -f $(D_CROSS)/$(PF)/$(DEB_HOST_GNU_TYPE)/$(CROSS_GNU_TYPE)/lib/libctf*
+ifeq (,$(findstring stage2,$(DEB_BUILD_OPTIONS)))
 	rm -f $(D_CROSS)/$(PF)/lib/$(CROSS_GNU_TYPE)/libctf*
 	rm -f $(D_CROSS)/$(PF)/lib/$(CROSS_GNU_TYPE)/libsframe*
+endif
 	rm -f $(D_CROSS)/$(PF)/include/*.h
 
 	: # remove RPATH
--- a/debian/control.in
+++ b/debian/control.in
@@ -221,6 +221,29 @@
 Description: GNU binutils targeted for hppa64-linux (debug symbols)
  This package provides debug symbols for binutils-hppa64-linux-gnu.
 
+Package: binutils-i586-linux-gnu
+Architecture: i586
+Depends: ${shlibs:Depends}, binutils (= ${binary:Version}),
+  binutils-common (= ${binary:Version})
+Recommends: libc6-dev
+Provides: binutils-i586
+Suggests: binutils-doc (>= ${source:Version})
+Breaks: binutils-i586 (<< 2.25.1-2)
+Replaces: binutils-i586 (<< 2.25.1-2)
+Description: GNU assembler, linker and binary utilities targeted for i586-linux
+ The programs in this package are used to assemble, link and manipulate
+ binary and object files.  They may be used in conjunction with a compiler
+ and various libraries to build programs.
+ .
+ This package is needed to build an 32-bit kernel for i586 machines.
+
+Package: binutils-i586-linux-gnu-dbg
+Section: debug
+Architecture: i586
+Depends: binutils-i586-linux-gnu (= ${binary:Version})
+Description: GNU binutils targeted for i586-linux (debug symbols)
+ This package provides debug symbols for binutils-i586-linux-gnu.
+
 Package: binutils-doc
 Section: doc
 Architecture: all
EOF
		# Vortex: set Debian arch
		#echo "$HOST_ARCH" > debian/target
	fi
}

add_automatic blt
add_automatic bsdmainutils
patch_bsdmainutils() {
	dpkg-architecture "-a$HOST_ARCH" -imusl-linux-any || return 0
	echo "fixing FTBFS on musl-linux-any #989688"
	drop_privs sed -i -e '/__unused/d' freebsd.h
}

builddep_build_essential() {
	# g++ dependency needs cross translation
	apt_get_install debhelper python3
}
patch_build_essential() {
	# Add new arch
	if [ "$HOST_ARCH" = "i586" ]; then
		echo "i586" | drop_privs tee -a debian/cross-targets >/dev/null
		drop_privs cp essential-packages-list-i386 essential-packages-list-i586
		drop_privs patch -p1 <<'EOF'
--- a/debian/control
+++ b/debian/control
@@ -126,6 +126,25 @@
  to build-depend on, you can always leave out the packages this
  package depends on.
 
+Package: crossbuild-essential-i586
+Architecture: all
+Depends: ${cross-essential}, ${misc:Depends}
+Description: Informational list of cross-build-essential packages
+ If you do not plan to cross build Debian packages, you don't need
+ this package.  Starting with sbuild (>= 0.63.0) this package is
+ required for cross building Debian packages in a chroot.
+ .
+ This package contains an informational list of packages which are
+ considered essential for cross building Debian packages.  This
+ package also depends on the packages on that list, to make it easy to
+ have the cross-build-essential packages installed.
+ .
+ If you have this package installed, you only need to install whatever
+ a package specifies as its build-time dependencies to cross build the
+ package.  Conversely, if you are determining what your package needs
+ to build-depend on, you can always leave out the packages this
+ package depends on.
+
 Package: crossbuild-essential-powerpc
 Architecture: all
 Depends: ${cross-essential}, ${misc:Depends}
EOF
	fi # i586
}

add_automatic bzip2
builddep_bzip2() {
	# Fix: Unmet build dependencies: docbook-xml docbook2x xsltproc
	# Also set "nodoc"
	profiles="$profiles,nodoc"
	apt_get_build_dep "-a$HOST_ARCH" --arch-only "-P$profiles" ./
}
add_automatic c-ares
add_automatic coreutils
add_automatic curl
patch_curl() {
	if test "$ENABLE_MULTIARCH_GCC" != yes; then
		drop_privs patch -p1 <<'EOF'
--- a/debian/rules
+++ a/debian/rules
@@ -10,7 +10,7 @@
 	--disable-symbol-hiding --enable-versioned-symbols	\
 	--enable-threaded-resolver --with-lber-lib=lber		\
 	--with-gssapi=/usr --with-nghttp2	\
-	--includedir=/usr/include/$(DEB_HOST_MULTIARCH)		\
+	--includedir=/usr/$(DEB_HOST_MULTIARCH)/include		\
 	--with-zsh-functions-dir=/usr/share/zsh/vendor-completions
 
 # disable libssh2 on Ubuntu (see #888449)
@@ -73,6 +73,7 @@
 	${MAKE} -C debian/build-nss				\
 		DESTDIR=$(shell pwd)/debian/tmp-nss install
 	find debian/tmp* -name '*.la' -delete
+	sed -i -e "s|usr/include|usr/$(DEB_HOST_MULTIARCH)/include|g" debian/*.install
 	dh_install -plibcurl3-gnutls -plibcurl4-gnutls-dev	\
 		--sourcedir=debian/tmp-gnutls
 	dh_install -plibcurl3-nss -plibcurl4-nss-dev		\
EOF
	fi
}
add_automatic dash
add_automatic db-defaults
add_automatic debianutils

add_automatic diffutils
buildenv_diffutils() {
	if dpkg-architecture "-a$1" -ignu-any-any; then
		export gl_cv_func_getopt_gnu=yes
	fi
}
patch_diffutils() {
	test "$HOST_ARCH" = loong64 || return 0
	echo "Add loong support #1029275"
	drop_privs patch -p1 <<'EOF'
--- a/m4/stack-direction.m4
+++ b/m4/stack-direction.m4
@@ -37,6 +37,7 @@
       m88k | \
       mcore | \
       microblaze | \
+      loongarch* | \
       mips* | \
       mmix | \
       mn10200 | \
EOF
}

add_automatic dpkg
patch_dpkg() {
	# Add i586
	drop_privs patch -p1 <<'EOF'
--- dpkg-1.21.22/data/cputable
+++ dpkg-1.21.22/data/cputable
@@ -27,7 +27,8 @@
 avr32		avr32		avr32			32	big
 hppa		hppa		hppa.*			32	big
 loong64		loongarch64	loongarch64		64	little
-i386		i686		(i[34567]86|pentium)	32	little
+i386		i686		(i[3467]86|pentium)	32	little
+i586		i586		(i586|vortex)		32	little
 ia64		ia64		ia64			64	little
 m32r		m32r		m32r			32	big
 m68k		m68k		m68k			32	big
# i586 is not a multilib of i686
--- dpkg-1.21.22/scripts/Dpkg/Arch.pm
+++ dpkg-1.21.22/scripts/Dpkg/Arch.pm
@@ -395,7 +395,7 @@
     my $gnu = shift;
     my ($cpu, $cdr) = split(/-/, $gnu, 2);
 
-    if ($cpu =~ /^i[4567]86$/) {
+    if ($cpu =~ /^i[467]86$/) {
 	return "i386-$cdr";
     } else {
 	return $gnu;
EOF
}
add_automatic dosfstools
add_automatic dwz
add_automatic e2fsprogs
add_automatic expat
patch_expat() {
	# #1037080 (fixed in 2.5.0-2)
	# Fix chmod: cannot access ... libexpat1-dev/examples/*
	drop_privs patch -p1 <<'EOF'
--- expat-2.5.0/debian/rules
+++ expat-2.5.0/debian/rules
@@ -14,7 +14,13 @@
 
 ifneq (,$(filter stage1, $(DEB_BUILD_PROFILES)))
   without_docbook = --without-docbook
-  build_targets = lib xmlwf tests examples
+  build_targets = lib xmlwf
+ifeq (,$(filter nocheck, $(DEB_BUILD_PROFILES)))
+  build_targets += tests
+endif
+ifeq (,$(filter nodoc, $(DEB_BUILD_PROFILES)))
+  build_targets += examples
+endif
 endif
 
 ifeq (,$(filter noudeb, $(DEB_BUILD_PROFILES)))
@@ -123,14 +129,18 @@
 	dh_installdocs -a
 	dh_installman -a
 #	dh_installman -pexpat debian/xmlwf.1
+ifeq (,$(filter nodoc, $(DEB_BUILD_PROFILES)))
 	dh_installexamples -a
+endif
 	dh_installchangelogs -a expat/Changes
 	dh_link -a
 	dh_strip -a
 	dh_compress -a
 	dh_fixperms -a
-	chmod 644 $(CURDIR)/debian/libexpat1-dev/usr/share/doc/libexpat1-dev/examples/* \
-	          $(CURDIR)/debian/libexpat1-dev/usr/share/aclocal/*
+ifeq (,$(filter nodoc, $(DEB_BUILD_PROFILES)))
+	chmod 644 $(CURDIR)/debian/libexpat1-dev/usr/share/doc/libexpat1-dev/examples/*
+endif
+	chmod 644 $(CURDIR)/debian/libexpat1-dev/usr/share/aclocal/*
 	dh_makeshlibs -a -V $(if $(with_udeb),--add-udeb=$(UPACKAGE))
 	dh_installdeb -a
 	dh_shlibdeps -a -l $(CURDIR)/debian/libexpat1/usr/lib/$(DEB_HOST_MULTIARCH)
EOF
}
add_automatic file
add_automatic findutils
add_automatic flex
builddep_flex() {
	# Fix for "all"
	# Fix: Unmet build dependencies: texlive-latex-base texlive-fonts-recommended cm-super-minimal
	# Also set "nodoc"
	#profiles="$profiles,nodoc"
	# Depends for Cross
	apt_get_build_dep "-a$HOST_ARCH" --arch-only "-P$2" ./
	# Depends for build
	apt_get_build_dep "-P$2" ./
}
add_automatic fontconfig
add_automatic freetype
add_automatic fribidi
add_automatic fuse

patch_gcc_default_pie_everywhere()
{
	echo "enabling pie everywhere #892281"
	drop_privs patch -p1 <<'EOF'
--- a/debian/rules.defs
+++ a/debian/rules.defs
@@ -1250,9 +1250,7 @@
     pie_archs += armhf arm64 i386
   endif
 endif
-ifneq (,$(filter $(DEB_TARGET_ARCH),$(pie_archs)))
-  with_pie := yes
-endif
+with_pie := yes
 ifeq ($(trunk_build),yes)
   with_pie := disabled for trunk builds
 endif
EOF
}
patch_gcc_limits_h_test() {
	echo "fix LIMITS_H_TEST again https://gcc.gnu.org/bugzilla/show_bug.cgi?id=80677"
	drop_privs tee debian/patches/limits-h-test.diff >/dev/null <<'EOF'
--- a/src/gcc/limitx.h
+++ b/src/gcc/limitx.h
@@ -29,7 +29,7 @@
 #ifndef _GCC_LIMITS_H_  /* Terminated in limity.h.  */
 #define _GCC_LIMITS_H_

-#ifndef _LIBC_LIMITS_H_
+#if !defined(_LIBC_LIMITS_H_) && __has_include_next(<limits.h>)
 /* Use "..." so that we find syslimits.h only in this same directory.  */
 #include "syslimits.h"
 #endif
--- a/src/gcc/limity.h
+++ b/src/gcc/limity.h
@@ -3,7 +3,7 @@

 #else /* not _GCC_LIMITS_H_ */

-#ifdef _GCC_NEXT_LIMITS_H
+#if defined(_GCC_NEXT_LIMITS_H) && __has_include_next(<limits.h>)
 #include_next <limits.h>		/* recurse down to the real one */
 #endif

--- a/src/gcc/Makefile.in
+++ b/src/gcc/Makefile.in
@@ -3139,11 +3139,7 @@
 	  sysroot_headers_suffix=`echo $${ml} | sed -e 's/;.*$$//'`; \
 	  multi_dir=`echo $${ml} | sed -e 's/^[^;]*;//'`; \
 	  include_dir=include$${multi_dir}; \
-	  if $(LIMITS_H_TEST) ; then \
-	    cat $(srcdir)/limitx.h $(T_GLIMITS_H) $(srcdir)/limity.h > tmp-xlimits.h; \
-	  else \
-	    cat $(T_GLIMITS_H) > tmp-xlimits.h; \
-	  fi; \
+	  cat $(srcdir)/limitx.h $(T_GLIMITS_H) $(srcdir)/limity.h > tmp-xlimits.h; \
 	  $(mkinstalldirs) $${include_dir}; \
 	  chmod a+rx $${include_dir} || true; \
 	  $(SHELL) $(srcdir)/../move-if-change \
EOF
	if test "$GCC_VER" -le 12; then
		drop_privs sed -i -e 's/include_dir=include/fix_dir=include-fixed/' -e 's/{include_dir}/{fix_dir}/' debian/patches/limits-h-test.diff
	fi
	echo "debian_patches += limits-h-test" | drop_privs tee -a debian/rules.patch >/dev/null
}
patch_gcc_unapplicable_ada() {
	echo "fix patch application failure #993205"
	drop_privs sed -i -e /ada-armel-libatomic/d debian/rules.patch
}
patch_gcc_crypt_h() {
	echo "fix libsanitizer failing to find crypt.h #1014375"
	drop_privs patch -p1 <<'EOF'
--- a/debian/rules2
+++ b/debian/rules2
@@ -1251,6 +1251,13 @@
 	  mkdir -p $(builddir)/sys-include; \
 	  ln -sf /usr/include/$(DEB_TARGET_MULTIARCH)/asm $(builddir)/sys-include/asm; \
 	fi
+	: # Fall back to the host crypt.h when target is unavailable as the sizeof(struct crypt_data) is unlikely to change, needed by libsanitizer.
+	if [ ! -f /usr/include/crypt.h ] && \
+		[ ! -f /usr/include/$(DEB_TARGET_MULTIARCH)/crypt.h ] && \
+		[ -f /usr/include/$(DEB_HOST_MULTIARCH)/crypt.h ]; then \
+	  mkdir -p $(builddir)/sys-include; \
+	  ln -sf /usr/include/$(DEB_HOST_MULTIARCH)/crypt.h $(builddir)/sys-include/crypt.h; \
+	fi

 	touch $(configure_stamp)

EOF
}
patch_gcc_has_include_next() {
	dpkg-architecture "-a$HOST_ARCH" -ihurd-any || return 0
	echo "fix __has_include_next https://gcc.gnu.org/bugzilla/show_bug.cgi?id=80755"
	drop_privs tee debian/patches/has_include_next.diff >/dev/null <<EOF
--- a/src/libcpp/files.cc
+++ b/src/libcpp/files.cc
@@ -1042,7 +1042,7 @@
      path use the normal search logic.  */
   if (type == IT_INCLUDE_NEXT && file->dir
       && file->dir != &pfile->no_search_path)
-    dir = file->dir->next;
+    return file->dir->next;
   else if (angle_brackets)
     dir = pfile->bracket_include;
   else if (type == IT_CMDLINE)
@@ -2145,6 +2145,8 @@
 		 enum include_type type)
 {
   cpp_dir *start_dir = search_path_head (pfile, fname, angle_brackets, type);
+  if (!start_dir)
+    return false;
   _cpp_file *file = _cpp_find_file (pfile, fname, start_dir, angle_brackets,
 				    _cpp_FFK_HAS_INCLUDE, 0);
   return file->err_no != ENOENT;
EOF
	echo "debian_patches += has_include_next" | drop_privs tee -a debian/rules.patch >/dev/null
}
patch_gcc_loong64() {
	test "$HOST_ARCH" = loong64 || return 0
	echo "revert loong64 tuple #1031850"
	drop_privs tee debian/patches/loong64_tuple.diff >/dev/null <<'EOF'
--- a/src/gcc/config/loongarch/t-linux
+++ b/src/gcc/config/loongarch/t-linux
@@ -32,14 +32,14 @@ ifneq ($(call if_multiarch,yes),yes)
 else
     # Only define MULTIARCH_DIRNAME when multiarch is enabled,
     # or it would always introduce ${target} into the search path.
-    MULTIARCH_DIRNAME = $(call if_multiarch,loongarch64-linux-gnuf64)
+    MULTIARCH_DIRNAME = $(LA_MULTIARCH_TRIPLET)
 endif

 # Don't define MULTILIB_OSDIRNAMES if multilib is disabled.
 ifeq ($(filter LA_DISABLE_MULTILIB,$(tm_defines)),)

     MULTILIB_OSDIRNAMES = \
-      mabi.lp64d=../lib$(call if_multiarch,:loongarch64-linux-gnuf64)
+      mabi.lp64d=../lib$(call if_multiarch,:loongarch64-linux-gnu)

     MULTILIB_OSDIRNAMES += \
       mabi.lp64f=../lib/f32$(call if_multiarch,:loongarch64-linux-gnuf32)
EOF
	echo "debian_patches += loong64_tuple" | drop_privs tee -a debian/rules.patch >/dev/null
}
patch_gcc_wdotap() {
	if test "$ENABLE_MULTIARCH_GCC" = yes; then
		echo "applying patches for with_deps_on_target_arch_pkgs"
		drop_privs rm -Rf .pc
		drop_privs QUILT_PATCHES="/usr/share/cross-gcc/patches/gcc-$GCC_VER" quilt push -a
		drop_privs rm -Rf .pc
	fi
}
patch_gcc_i586() {
	if [ "$HOST_ARCH" = "i586" ]; then
		# Vortex: i586 was a copy from i386. This should set the last include dir
		# Append to existing patch
		drop_privs tee -a debian/patches/gcc-multiarch.diff <<'EOF'
--- a/src/gcc/config/i386/t-linux
+++ b/src/gcc/config/i386/t-linux
@@ -1 +1 @@
-MULTIARCH_DIRNAME = $(call if_multiarch,i386-linux-gnu)
+MULTIARCH_DIRNAME = $(call if_multiarch,i586-linux-gnu)
--- a/src/gcc/config/i386/t-linux64
+++ b/src/gcc/config/i386/t-linux64
@@ -54,5 +54,5 @@
   MULTIARCH_DIRNAME = $(call if_multiarch,x86_64-linux-gnu)
   endif
 else
-  MULTIARCH_DIRNAME = $(call if_multiarch,i386-linux-gnu)
+  MULTIARCH_DIRNAME = $(call if_multiarch,i586-linux-gnu)
 endif
 
EOF
	fi
	# Add build date to gcc -v
	drop_privs patch -p1 <<'EOF'
--- gcc/debian/rules2
+++ gcc/debian/rules2
@@ -204,7 +204,7 @@
 endif
 
 CONFARGS = -v \
-	--with-pkgversion='$(distribution)$(if $(with_ibm_branch),/IBM)___$(DEB_VERSION)' \
+	--with-pkgversion='$(distribution)$(if $(with_ibm_branch),/IBM)___$(DEB_VERSION)___$(shell date | sed "s/ /___/g")' \
 	--with-bugurl='file:///usr/share/doc/$(PKGSOURCE)/README.Bugs'
 
 CONFARGS += \
EOF
	if [ "$HOST_ARCH" = "i586" ]; then
		echo "Vortex: not map i586 to i686"
		drop_privs sed -i -e 's/subst i586,i686/subst i586_not_map,i686/' debian/rules.defs
	fi
}
patch_gcc_12() {
	patch_gcc_limits_h_test
	patch_gcc_default_pie_everywhere
	patch_gcc_unapplicable_ada
	patch_gcc_crypt_h
	patch_gcc_has_include_next
	patch_gcc_loong64
	patch_gcc_wdotap
	patch_gcc_i586
}
patch_gcc_13() {
	patch_gcc_limits_h_test
	patch_gcc_loong64
	patch_gcc_wdotap
}

buildenv_gdbm() {
	if dpkg-architecture "-a$1" -ignu-any-any; then
		export ac_cv_func_mmap_fixed_mapped=yes
	fi
}

add_automatic glib2.0
patch_glib2_0() {
	# Fix: ffi.h: No such file or directory
	drop_privs patch -p1 <<'EOF'
--- a/debian/rules
+++ a/debian/rules
@@ -35,6 +35,10 @@
 DEB_CONFIGURE_EXTRA_FLAGS += --cross-file=$(CURDIR)/debian/meson/stack-grows-down.ini
 endif
 
+ifneq ($(ENABLE_MULTIARCH_GCC), yes)
+export DEB_CFLAGS_MAINT_APPEND = -I/usr/include/$(DEB_TARGET_MULTIARCH)
+endif
+
 endif # cross-compiling
 
 ifneq ($(filter kfreebsd,$(DEB_HOST_ARCH_OS)),)
EOF
	dpkg-architecture "-a$HOST_ARCH" -ix32-any-any-any || return 0
	# https://github.com/mesonbuild/meson/issues/9845
	echo "working around wrong cc_can_run on x32"
	drop_privs tee -a debian/meson/libc-properties.ini >/dev/null <<EOF
needs_exe_wrapper=true
EOF
}

builddep_glibc() {
	test "$1" = "$HOST_ARCH"
	apt_get_install gettext file quilt autoconf gawk debhelper rdfind symlinks binutils bison netbase "gcc-$GCC_VER$HOST_ARCH_SUFFIX"
	case "$(dpkg-architecture "-a$1" -qDEB_HOST_ARCH_OS)" in
		linux)
			if test "$ENABLE_MULTIARCH_GCC" = yes; then
				apt_get_install "linux-libc-dev:$HOST_ARCH"
			else
				apt_get_install "linux-libc-dev-$HOST_ARCH-cross"
			fi
		;;
		hurd)
			apt_get_install "gnumach-dev:$1" "hurd-headers-dev:$1" "mig$HOST_ARCH_SUFFIX"
		;;
		*)
			echo "rebootstrap-error: unsupported kernel"
			exit 1
		;;
	esac
}
patch_glibc() {
	echo "patching glibc to pass -l to dh_shlibdeps for multilib"
	drop_privs patch -p1 <<'EOF'
diff -Nru glibc-2.19/debian/rules.d/debhelper.mk glibc-2.19/debian/rules.d/debhelper.mk
--- glibc-2.19/debian/rules.d/debhelper.mk
+++ glibc-2.19/debian/rules.d/debhelper.mk
@@ -109,7 +109,7 @@
 	./debian/shlibs-add-udebs $(curpass)
 
 	dh_installdeb -p$(curpass)
-	dh_shlibdeps -p$(curpass)
+	dh_shlibdeps $(if $($(lastword $(subst -, ,$(curpass)))_slibdir),-l$(CURDIR)/debian/$(curpass)/$($(lastword $(subst -, ,$(curpass)))_slibdir)) -p$(curpass)
 	dh_gencontrol -p$(curpass)
 	if [ $(curpass) = nscd ] ; then \
 		sed -i -e "s/\(Depends:.*libc[0-9.]\+\)-[a-z0-9]\+/\1/" debian/nscd/DEBIAN/control ; \
EOF
	echo "patching glibc to find standard linux headers"
	drop_privs patch -p1 <<'EOF'
diff -Nru glibc-2.19/debian/sysdeps/linux.mk glibc-2.19/debian/sysdeps/linux.mk
--- glibc-2.19/debian/sysdeps/linux.mk
+++ glibc-2.19/debian/sysdeps/linux.mk
@@ -16,7 +16,7 @@
 endif

 ifndef LINUX_SOURCE
-  ifeq ($(DEB_HOST_GNU_TYPE),$(DEB_BUILD_GNU_TYPE))
+  ifeq ($(shell dpkg-query --status linux-libc-dev-$(DEB_HOST_ARCH)-cross 2>/dev/null),)
     LINUX_HEADERS := /usr/include
   else
     LINUX_HEADERS := /usr/$(DEB_HOST_GNU_TYPE)/include
EOF
	if ! sed -n '/^libc6_archs *:=/,/[^\\]$/p' debian/rules.d/control.mk | grep -qw "$HOST_ARCH"; then
		echo "adding $HOST_ARCH to libc6_archs"
		drop_privs sed -i -e "s/^libc6_archs *:= /&$HOST_ARCH /" debian/rules.d/control.mk
		drop_privs ./debian/rules debian/control
	fi
	echo "patching glibc to drop dev package conflict"
	sed -i -e '/^Conflicts: @libc-dev-conflict@$/d' debian/control.in/libc
	echo "patching glibc to move all headers to multiarch locations #798955"
	drop_privs patch -p1 <<'EOF'
--- a/debian/rules.d/build.mk
+++ b/debian/rules.d/build.mk
@@ -4,12 +4,16 @@
 xx=$(if $($(curpass)_$(1)),$($(curpass)_$(1)),$($(1)))
 define generic_multilib_extra_pkg_install
 set -e; \
-mkdir -p debian/$(1)/usr/include/sys; \
-ln -sf $(DEB_HOST_MULTIARCH)/bits debian/$(1)/usr/include/; \
-ln -sf $(DEB_HOST_MULTIARCH)/gnu debian/$(1)/usr/include/; \
-ln -sf $(DEB_HOST_MULTIARCH)/fpu_control.h debian/$(1)/usr/include/; \
-for i in `ls debian/tmp/usr/include/$(DEB_HOST_MULTIARCH)/sys`; do \
-	ln -sf ../$(DEB_HOST_MULTIARCH)/sys/$$i debian/$(1)/usr/include/sys/$$i; \
+mkdir -p debian/$(1)/usr/include; \
+for i in `ls debian/tmp/usr/include/$(DEB_HOST_MULTIARCH)`; do \
+	if test -d "debian/tmp/usr/include/$(DEB_HOST_MULTIARCH)/$$i" && ! test "$$i" = bits -o "$$i" = gnu; then \
+		mkdir -p "debian/$(1)/usr/include/$$i"; \
+		for j in `ls debian/tmp/usr/include/$(DEB_HOST_MULTIARCH)/$$i`; do \
+			ln -sf "../$(DEB_HOST_MULTIARCH)/$$i/$$j" "debian/$(1)/usr/include/$$i/$$j"; \
+		done; \
+	else \
+		ln -sf "$(DEB_HOST_MULTIARCH)/$$i" "debian/$(1)/usr/include/$$i"; \
+	fi; \
 done
 mkdir -p debian/$(1)/usr/include/finclude; \
 for i in `ls debian/tmp/usr/include/finclude/$(DEB_HOST_MULTIARCH)`; do \
@@ -270,15 +272,11 @@
 	    echo "/lib/$(DEB_HOST_GNU_TYPE)" >> $$conffile; \
 	    echo "/usr/lib/$(DEB_HOST_GNU_TYPE)" >> $$conffile; \
 	  fi; \
-	  mkdir -p $(debian-tmp)/usr/include/$(DEB_HOST_MULTIARCH); \
-	  mv $(debian-tmp)/usr/include/bits $(debian-tmp)/usr/include/$(DEB_HOST_MULTIARCH); \
-	  mv $(debian-tmp)/usr/include/gnu $(debian-tmp)/usr/include/$(DEB_HOST_MULTIARCH); \
-	  mv $(debian-tmp)/usr/include/sys $(debian-tmp)/usr/include/$(DEB_HOST_MULTIARCH); \
-	  mv $(debian-tmp)/usr/include/fpu_control.h $(debian-tmp)/usr/include/$(DEB_HOST_MULTIARCH); \
-	  mv $(debian-tmp)/usr/include/a.out.h $(debian-tmp)/usr/include/$(DEB_HOST_MULTIARCH); \
-	  mv $(debian-tmp)/usr/include/ieee754.h $(debian-tmp)/usr/include/$(DEB_HOST_MULTIARCH); \
+	  mkdir -p $(debian-tmp)/usr/include.tmp; \
+	  mv $(debian-tmp)/usr/include $(debian-tmp)/usr/include.tmp/$(DEB_HOST_MULTIARCH); \
+	  mv $(debian-tmp)/usr/include.tmp $(debian-tmp)/usr/include; \
 	  mkdir -p $(debian-tmp)/usr/include/finclude/$(DEB_HOST_MULTIARCH); \
-	  mv $(debian-tmp)/usr/include/finclude/math-vector-fortran.h $(debian-tmp)/usr/include/finclude/$(DEB_HOST_MULTIARCH); \
+	  mv $(debian-tmp)/usr/include/$(DEB_HOST_MULTIARCH)/finclude/math-vector-fortran.h $(debian-tmp)/usr/include/finclude/$(DEB_HOST_MULTIARCH); \
 	fi
 
 	ifeq ($(filter stage1,$(DEB_BUILD_PROFILES)),)
--- a/debian/sysdeps/hurd-i386.mk
+++ b/debian/sysdeps/hurd-i386.mk
@@ -18,9 +18,6 @@ endif
 define libc_extra_install
 mkdir -p $(debian-tmp)/lib
 ln -s ld.so.1 $(debian-tmp)/lib/ld.so
-mkdir -p $(debian-tmp)/usr/include/$(DEB_HOST_MULTIARCH)/mach
-mv $(debian-tmp)/usr/include/mach/i386 $(debian-tmp)/usr/include/$(DEB_HOST_MULTIARCH)/mach/
-ln -s ../$(DEB_HOST_MULTIARCH)/mach/i386 $(debian-tmp)/usr/include/mach/i386
 endef
 
 # FIXME: We are having runtime issues with ifunc...
EOF
	echo "patching glibc to avoid -Werror"
	drop_privs patch -p1 <<'EOF'
--- a/debian/rules.d/build.mk
+++ b/debian/rules.d/build.mk
@@ -85,6 +85,7 @@
 		$(CURDIR)/configure \
 		--host=$(call xx,configure_target) \
 		--build=$$configure_build --prefix=/usr \
+		--disable-werror \
 		--enable-add-ons=$(standard-add-ons)"$(call xx,add-ons)" \
 		--without-selinux \
 		--enable-stackguard-randomization \
EOF
	if test "$ENABLE_MULTIARCH_GCC" = yes; then
		echo "Vortetx:i586 w/o multilib, but add multiarch"
		drop_privs patch -p1 <<'EOF'
--- /dev/null
+++ b/debian/sysdeps/i586.mk
@@ -0,0 +1,6 @@
+# configuration options for all flavours
+extra_config_options = --enable-multi-arch
+CC = $(DEB_HOST_GNU_TYPE)-$(BASE_CC)$(DEB_GCC_VERSION) -Wl,--hash-style=both
+CXX = $(DEB_HOST_GNU_TYPE)-$(BASE_CXX)$(DEB_GCC_VERSION) -Wl,--hash-style=both
+
+# w/o multilib flavours
# Fix DEFAULT_MEMCMP __memcmp_ia32
# ... libc.a(dl-sysdep.o): in function `_dl_tunable_set_hwcaps':
# ... sysdeps/unix/sysv/linux/x86/dl-sysdep.c:207: undefined reference to `__memcmp_ia32'
--- glibc-2.36/sysdeps/x86/cpu-tunables.c
+++ glibc-2.36/sysdeps/x86/cpu-tunables.c
@@ -29,10 +29,13 @@
 /* We can't use IFUNC memcmp nor strlen in init_cpu_features from libc.a
    since IFUNC must be set up by init_cpu_features.  */
 # if defined USE_MULTIARCH && !defined SHARED
-#  ifdef __x86_64__
+#  if defined (__i586__)
+#   define DEFAULT_MEMCMP	memcmp
+#  elif defined (__x86_64__)
 /* DEFAULT_MEMCMP by sysdeps/x86_64/memcmp-isa-default-impl.h.  */
 #   include <sysdeps/x86_64/memcmp-isa-default-impl.h>
 #  else
+# error "Bug: Bad default __memcmp_ia32 implementation for i586"
 #   define DEFAULT_MEMCMP	__memcmp_ia32
 #  endif
 extern __typeof (memcmp) DEFAULT_MEMCMP;
EOF
	fi
}
buildenv_glibc() {
	export DEB_GCC_VERSION="-$GCC_VER"
}

add_automatic gmp
patch_gmp() {
	drop_privs patch -p1 <<'EOF'
--- a/debian/rules
+++ b/debian/rules
@@ -98,10 +98,18 @@
 	rm -rf debian/tmp
 	# Install places gmp.h in 'includeexecdir' which is non-standard and cannot be set at compile time,
 	# so override it at install.
+ifeq ($(ENABLE_MULTIARCH_GCC), yes)
 	$(MAKE) DESTDIR=`pwd`/debian/tmp includeexecdir=/usr/include/$(DEB_HOST_MULTIARCH) -C build install
+else
+	$(MAKE) DESTDIR=`pwd`/debian/tmp includeexecdir=/usr/$(DEB_HOST_MULTIARCH)/include -C build install
+endif
 
 	# Replace embedded build path with a placeholder string
+ifeq ($(ENABLE_MULTIARCH_GCC), yes)
 	sed -i -e "s,$(CURDIR),BUILDPATH,g" debian/tmp/usr/include/$(DEB_HOST_MULTIARCH)/gmp.h
+else
+	sed -i -e "s,$(CURDIR),BUILDPATH,g" debian/tmp/usr/$(DEB_HOST_MULTIARCH)/include/gmp.h
+endif
 
 	dh_install -plibgmp10 usr/lib/*/libgmp.so.*
 	dh_install -plibgmpxx4ldbl usr/lib/*/libgmpxx.so.*
@@ -110,6 +118,9 @@
 	dh_install -plibgmp-dev usr/lib/*/lib*.a
 	dh_install -plibgmp-dev usr/lib/*/pkgconfig
 	dh_install -plibgmp-dev usr/include
+ifneq ($(ENABLE_MULTIARCH_GCC), yes)
+	dh_install -plibgmp-dev usr/$(DEB_HOST_MULTIARCH)/include
+endif
 
 	# Install upstream ChangeLog, AUTHORS, NEWS, and README only in -dev package
 	dh_installchangelogs -plibgmp-dev
EOF
}

builddep_gnu_efi() {
	# binutils dependency needs cross translation
	apt_get_install debhelper
}

add_automatic gnupg2
builddep_gnupg2() {
	apt_get_install libassuan-dev:$HOST_ARCH libbz2-dev:$HOST_ARCH libcurl4-gnutls-dev:$HOST_ARCH libgcrypt20-dev:$HOST_ARCH libgnutls28-dev:$HOST_ARCH libgpg-error-dev:$HOST_ARCH libksba-dev:$HOST_ARCH libldap2-dev:$HOST_ARCH libnpth0-dev:$HOST_ARCH libreadline-dev:$HOST_ARCH libsqlite3-dev:$HOST_ARCH libusb-1.0-0-dev:$HOST_ARCH
}
buildenv_gnupg2() {
	# "checking for GPG Error" failed. Fix "libgpg-error is needed"
	local triplet=`dpkg-architecture -a$HOST_ARCH -qDEB_HOST_MULTIARCH`
	export gpgrt_libdir=/usr/lib/$triplet
	export ac_cv_path_GPGRT_CONFIG=/usr/bin/gpgrt-config
	export PKG_CONFIG_PATH=/usr/lib/$triplet/pkgconfig
}

add_automatic gpm
# gpgme1.0 can't build
#add_automatic gpgme1.0
builddep_gpgme1_0() {
	apt_get_build_dep "-a$HOST_ARCH" --arch-only "-P$2" ./ || :
	#apt_get_build_dep "-P$2" ./

	apt_get_remove libassuan-dev
	apt_get_install libassuan-dev:$HOST_ARCH libbz2-dev:$HOST_ARCH libcurl4-gnutls-dev:$HOST_ARCH libgcrypt20-dev:$HOST_ARCH libgnutls28-dev:$HOST_ARCH libgpg-error-dev:$HOST_ARCH libksba-dev:$HOST_ARCH libldap2-dev:$HOST_ARCH libnpth0-dev:$HOST_ARCH libreadline-dev:$HOST_ARCH libsqlite3-dev:$HOST_ARCH libusb-1.0-0-dev:$HOST_ARCH
}
patch_gpgme1_0() {
		drop_privs patch -p1 <<'EOF'
--- gpgme1.0-1.18.0/debian/rules
+++ gpgme1.0-1.18.0/debian/rules
@@ -17,7 +17,7 @@
 override_dh_auto_configure:
 	dh_auto_configure --builddirectory=build -- \
 	    --enable-static \
-	    --enable-languages="python cpp qt" \
+	    --enable-languages="python cpp" \
 	    --infodir=/usr/share/info
 
 override_dh_missing:
EOF
}
add_automatic grep
add_automatic groff

add_automatic guile-3.0
patch_guile_3_0() {
	dpkg-architecture "-a$HOST_ARCH" -imusl-any-any || return 0
	echo "fixing libc dependency #1008712"
	drop_privs sed -i -e s/libc6-dev/libc-dev/ debian/control
}

add_automatic gzip
patch_gzip() {
	test "$(dpkg-architecture "-a$HOST_ARCH" -qDEB_HOST_ARCH_BITS)" = 32 || return 0
	echo "fixing time_t ftcbfs #1009893"
	drop_privs sed -i -e '/CONFIGURE_ARGS.*--host/s/$/ --build=${DEB_BUILD_GNU_TYPE}/' debian/rules

	echo "fix depend mingw-w64 and not build gzip.exe #1037094"
	if test "$ENABLE_MULTIARCH_GCC" = yes; then
		drop_privs patch -p1 <<'EOF'
--- gzip-1.12/debian/rules
+++ gzip-1.12/debian/rules
@@ -75,8 +75,6 @@
 configure-indep-stamp: reconf-stamp
 	dh_testdir
 	mkdir -p builddir-indep
-	cd builddir-indep && ../configure --host=i686-w64-mingw32 \
-		LIBS="-Wl,-Bstatic -lssp" --disable-silent-rules
 	:>$@
 
 build: build-stamp build-indep-stamp
@@ -95,7 +93,6 @@
 build-indep: build-indep-stamp
 build-indep-stamp: configure-indep-stamp
 	dh_testdir
-	${MAKE} -C builddir-indep
 	:>$@
 
 clean:
@@ -112,9 +109,6 @@
 	dh_testroot
 	dh_prep
 	dh_installdirs
-	i686-w64-mingw32-strip builddir-indep/gzip.exe
-	install -m 0755 builddir-indep/gzip.exe \
-	    debian/gzip-win32/usr/share/win32/
 	dh_installdocs -i README* TODO NEWS
 	dh_installchangelogs -i ChangeLog
 	dh_lintian -i
EOF
	fi
}
buildenv_gzip() {
	dpkg-architecture "-a$HOST_ARCH" -imusl-linux-any || return 0
	# this avoids replacing fseeko with a variant that is broken
	echo gl_cv_func_fflush_stdin exported
	export gl_cv_func_fflush_stdin=yes
}
builddep_gzip() {
	# Fix: Unmet build dependencies: mingw-w64
	dpkg-checkbuilddeps -B "-a$HOST_ARCH" "-P$2"
	#apt_get_build_dep "-a$HOST_ARCH" --arch-only "-P$2" ./
	profiles="$profiles,nodoc"
}

add_automatic hostname
add_automatic icu
add_automatic iproute2
builddep_iproute2() {
	apt_get_build_dep "-a$HOST_ARCH" --arch-only "-P$2" ./ || :
	# Todo: build libxtables-dev:i586, libmnl-dev:i586
	apt_get_install libxtables-dev libatm1-dev:$HOST_ARCH libdb5.3-dev:$HOST_ARCH
}
patch_iproute2() {
	drop_privs patch -p1 <<'EOF'
--- iproute2-6.1.0/debian/iproute2.install
+++ iproute2-6.1.0/debian/iproute2.install
@@ -2,20 +2,15 @@
 usr/include/iproute2/
 sbin/arpd /usr/sbin
 sbin/bridge
-sbin/dcb
-sbin/devlink
 sbin/genl /usr/sbin
 sbin/ip /bin
 sbin/lnstat /usr/bin/
 sbin/nstat /usr/bin/
-sbin/rdma /usr/bin
 sbin/routel /usr/bin
 sbin/rtacct
 sbin/rtmon
 sbin/ss /bin
 sbin/tc
-sbin/tipc
-sbin/vdpa
 usr/lib/*/tc
 usr/share/bash-completion/
 usr/share/man/
--- iproute2-6.1.0/debian/rules
+++ iproute2-6.1.0/debian/rules
@@ -31,9 +31,11 @@
 execute_after_dh_clean:
 	make distclean
 
+ifeq (,$(filter cross, $(DEB_BUILD_PROFILES)))
 override_dh_shlibdeps:
 	dh_shlibdeps -a -Xq_atm.so -Xm_xt.so -Xm_ipt.so
 	dh_shlibdeps -a -- -pipmods -dRecommends -e debian/iproute2/usr/lib/$(DEB_HOST_MULTIARCH)/tc/m_xt.so -e debian/iproute2/usr/lib/$(DEB_HOST_MULTIARCH)/tc/q_atm.so -xlibc6
+endif
 
 override_dh_auto_test:
 	# upstream test suite needs root and leaves machine unclean, skip it
EOF
}

add_automatic isl-0.18
add_automatic jansson
add_automatic jemalloc
add_automatic keyutils
add_automatic kmod

add_automatic krb5
builddep_krb5() {
	# Fix for "all"
	apt_get_build_dep "-a$HOST_ARCH" --arch-only "-P$2" ./
	# checking for libverto... no
	# configure: error: cannot detect system libverto
	apt_get_install libverto-dev:$HOST_ARCH
}
buildenv_krb5() {
	export krb5_cv_attr_constructor_destructor=yes,yes
	export ac_cv_func_regcomp=yes
	export ac_cv_printf_positional=yes
}

add_automatic libassuan
builddep_libassuan() {
	# Build-Depends: debhelper-compat (= 13), libgpg-error-dev (>= 1.33)
	# Build-Depends-Indep: libgpg-error-mingw-w64-dev, mingw-w64
	
	apt_get_build_dep "-a$HOST_ARCH" --arch-only "-P$2" ./
	if [ "$BUILD_TYPE" = "any,all" ]; then
		apt_get_build_dep "-P$2" ./
	fi

	# Fix: # checking for gpg-error-config... no
	apt_get_install gpgrt-tools libgpg-error-dev:$HOST_ARCH
}

add_automatic linux-atm
builddep_linux_atm() {
	# Perl for Build, not Target
	apt_get_build_dep "-a$HOST_ARCH" --arch-only "-P$2" ./ || :
	apt_get_remove perl:$HOST_ARCH
	apt_get_install bison flex autoconf automake libtool perl libfl-dev
}
add_automatic libarchive
add_automatic libatomic-ops

add_automatic libbpf
add_automatic libbsd
patch_libbsd() {
	dpkg-architecture "-a$HOST_ARCH" -imusl-any-any || return 0
	echo "fix musl FTBFS #1032159"
	drop_privs patch -p1 <<'EOF'
--- a/debian/libbsd-dev.install
+++ b/debian/libbsd-dev.install
@@ -1,4 +1,3 @@
-usr/lib/*/libbsd-ctor.a
 usr/lib/*/libbsd.a
 usr/lib/*/libbsd.so
 usr/lib/*/pkgconfig/*.pc
--- a/debian/rules
+++ b/debian/rules
@@ -6,8 +6,15 @@
 export DEB_BUILD_MAINT_OPTIONS = hardening=+all
 export DEB_CFLAGS_MAINT_PREPEND = -Wall

+include /usr/share/dpkg/architecture.mk
+
 %:
 	dh $@

 override_dh_installchangelogs:
 	dh_installchangelogs --no-trim
+
+ifneq ($(DEB_HOST_ARCH_LIBC),musl)
+execute_after_dh_install:
+	dh_install -plibbsd-dev usr/lib/*/libbsd-ctor.a
+endif
EOF
}

add_automatic libcap2
add_automatic libdebian-installer
add_automatic libev

add_automatic libevent
buildenv_libevent() {
	echo "work around ftbfs #1023284"
	export DPKG_GENSYMBOLS_CHECK_LEVEL=0
}

add_automatic libffi
patch_libffi() {
	echo "fix symbols for loong64 #1024359"
	drop_privs sed -i '/)LIBFFI_COMPLEX_8\.0 /s/)/ !loong64)/' debian/libffi8.symbols
	drop_privs patch -p1 <<'EOF'
--- a/debian/rules
+++ b/debian/rules
@@ -109,9 +109,15 @@
 endif
 	dh_install -a --sourcedir=debian/tmp
 
+ifeq ($(ENABLE_MULTIARCH_GCC),yes)
 	mkdir -p debian/libffi-dev/usr/include/$(DEB_HOST_MULTIARCH)
 	mv debian/libffi-dev/usr/include/*.h \
 		debian/libffi-dev/usr/include/$(DEB_HOST_MULTIARCH)/
+else
+	mkdir -p debian/libffi-dev/usr/$(DEB_HOST_MULTIARCH)/include
+	mv debian/libffi-dev/usr/include/*.h \
+		debian/libffi-dev/usr/$(DEB_HOST_MULTIARCH)/include/
+endif
 
 
 # Build architecture-independent files here.
EOF
}

add_automatic libgc
buildenv_libgc() {
	if dpkg-architecture "-a$1" -imusl-linux-any; then
		echo "ignoring symbol differences for musl for now"
		export DPKG_GENSYMBOLS_CHECK_LEVEL=0
	fi
	if test "$1" = arc; then
		echo "ignoring symbol differences for arc #994211"
		export DPKG_GENSYMBOLS_CHECK_LEVEL=0
	fi
}

add_automatic libgcrypt20
buildenv_libgcrypt20() {
	export ac_cv_sys_symbol_underscore=no

	# Fix: "checking for GPG Error" failed, "libgpg-error is needed"
	if test "$ENABLE_MULTIARCH_GCC" != yes; then
		local triplet=`dpkg-architecture -a$HOST_ARCH -qDEB_HOST_MULTIARCH`
		export gpgrt_libdir=/usr/lib/$triplet
		export ac_cv_path_GPGRT_CONFIG=/usr/bin/gpgrt-config
		export PKG_CONFIG_PATH=/usr/lib/$triplet/pkgconfig
	fi
}
builddep_libgcrypt20() {
	# libgpg-error needs, exist in gpgrt-tools
	apt_get_install gpgrt-tools libgpg-error-dev:$HOST_ARCH
	# buildtype=any,all will build i686-w64-mingw32
	buildtype=any
}

add_automatic libgpg-error
patch_libgpg_error() {
	if test "$ENABLE_MULTIARCH_GCC" != yes; then
		drop_privs patch -p1 <<'EOF'
--- a/debian/libgpg-error-dev.install
+++ b/debian/libgpg-error-dev.install
@@ -1,5 +1,5 @@
 usr/bin/gpgrt-config
-usr/include/* usr/include/${DEB_HOST_MULTIARCH}/
+usr/include/* usr/${DEB_HOST_MULTIARCH}/include/
 usr/lib/*/libgpg-error.a
 usr/lib/*/libgpg-error.so
 usr/lib/*/pkgconfig/gpg-error.pc
EOF
	fi
}

add_automatic libice
add_automatic libidn

add_automatic libidn2
patch_libidn2() {
	dpkg-architecture "-a$HOST_ARCH" -imusl-linux-any || return 0
	echo "patching gettext version for musl support #999510"
	drop_privs rm -f m4/gettext.m4
	drop_privs patch -p1 <<'EOF'
--- a/configure.ac
+++ b/configure.ac
@@ -90,7 +90,8 @@
 ])

 AM_GNU_GETTEXT([external])
-AM_GNU_GETTEXT_VERSION([0.19.3])
+AM_GNU_GETTEXT_REQUIRE_VERSION([0.19.8])
+AM_GNU_GETTEXT_VERSION([0.19.6])

 AX_CODE_COVERAGE

EOF
	# must be newer than configure.ac
	drop_privs touch doc/idn2.1
}

add_automatic libksba
buildenv_libksba() {
	# "checking for GPG Error" failed. Fix "libgpg-error is needed"
	if test "$ENABLE_MULTIARCH_GCC" != yes; then
		local triplet=`dpkg-architecture -a$HOST_ARCH -qDEB_HOST_MULTIARCH`
		export gpgrt_libdir=/usr/lib/$triplet
		export ac_cv_path_GPGRT_CONFIG=/usr/bin/gpgrt-config
		export PKG_CONFIG_PATH=/usr/lib/$triplet/pkgconfig
	fi
}
add_automatic libmd
add_automatic libmnl
add_automatic libnsl
add_automatic libonig
add_automatic libpipeline
add_automatic libpng1.6

buildenv_libprelude() {
	case $(dpkg-architecture "-a$HOST_ARCH" -qDEB_HOST_GNU_SYSTEM) in *gnu*)
		echo "glibc does not return NULL for malloc(0)"
		export ac_cv_func_malloc_0_nonnull=yes
	;; esac
}

add_automatic libpsl
add_automatic libpthread-stubs
add_automatic libsepol
add_automatic libsm
add_automatic libsodium
add_automatic libssh
patch_libssh() {
	# cmake: Could NOT find ZLIB (missing: ZLIB_LIBRARY) #796545
	drop_privs patch -p1 <<'EOF'
--- a/debian/rules
+++ a/debian/rules
@@ -1,6 +1,9 @@
 #!/usr/bin/make -f
 
 DEB_CMAKE_EXTRA_FLAGS := -DBUILD_STATIC_LIB=ON -DLIB_INSTALL_DIR=/usr/lib/$(DEB_HOST_MULTIARCH) -DUNIT_TESTING=$(if $(filter nocheck,$(DEB_BUILD_OPTIONS)),OFF,ON) -DWITH_GSSAPI=ON
+ifneq ($(ENABLE_MULTIARCH_GCC),yes)
+DEB_CMAKE_EXTRA_FLAGS += -DCMAKE_LIBRARY_ARCHITECTURE=$(DEB_HOST_MULTIARCH)
+endif
 
 export DEB_LDFLAGS_MAINT_APPEND = -Wl,-z,defs -Wl,-O1 -Wl,--as-needed
 export DEB_BUILD_MAINT_OPTIONS = hardening=+all
EOF
}
add_automatic libssh2
add_automatic libsystemd-dummy
add_automatic libtasn1-6
add_automatic libtextwrap
add_automatic libtirpc

builddep_libtool() {
	assert_built "zlib"
	test "$1" = "$HOST_ARCH"
	# gfortran dependency needs cross-translation
	# gnulib dependency lacks M-A:foreign
	apt_get_install debhelper file automake autoconf autotools-dev help2man texinfo "zlib1g-dev:$HOST_ARCH" gnulib
	# Can not install, if fortran was not build
	if ! echo "$GCC_NOLANG" | grep -q "fortran"; then
		apt_get_install "gfortran-$GCC_VER$HOST_ARCH_SUFFIX"
	fi
}

add_automatic libunistring
buildenv_libunistring() {
	if dpkg-architecture "-a$HOST_ARCH" -ignu-any-any; then
		echo "glibc does not prefer rwlock writers to readers"
		export gl_cv_pthread_rwlock_rdlock_prefer_writer=no
	fi
	echo "memchr and strstr generally work"
	export gl_cv_func_memchr_works=yes
	export gl_cv_func_strstr_works_always=yes
	export gl_cv_func_strstr_linear=yes
	if dpkg-architecture "-a$HOST_ARCH" -imusl-any-any; then
		echo "setting malloc/realloc do not return 0"
		export ac_cv_func_malloc_0_nonnull=yes
		export ac_cv_func_realloc_0_nonnull=yes
	fi
}
patch_libunistring() {
	dpkg-architecture "-a$HOST_ARCH" -imusl-any-any || return 0
	echo "update symbols for musl #1022846"
	drop_privs patch -p1 <<'EOF'
--- a/debian/libunistring2.symbols
+++ b/debian/libunistring2.symbols
@@ -162,10 +162,18 @@
  libunistring_gl_uninorm_decomp_chars_table@Base 0.9.7
  libunistring_gl_uninorm_decomp_index_table@Base 0.9.7
  libunistring_gl_uninorm_decompose_merge_sort_inplace@Base 0.9.7
- libunistring_glthread_once_multithreaded@Base 1.0
+ (arch=gnu-any-any)libunistring_glthread_once_multithreaded@Base 1.0
  libunistring_glthread_once_singlethreaded@Base 0.9.7
+ (arch=musl-any-any)libunistring_glthread_recursive_lock_destroy_multithreaded@Base 1.0-2
  libunistring_glthread_recursive_lock_init_multithreaded@Base 0.9.7
+ (arch=musl-any-any)libunistring_glthread_recursive_lock_lock_multithreaded@Base 1.0-2
+ (arch=musl-any-any)libunistring_glthread_recursive_lock_unlock_multithreaded@Base 1.0-2
+ (arch=musl-any-any)libunistring_glthread_rwlock_destroy_multithreaded@Base 1.0-2
  (arch=gnu-any-any)libunistring_glthread_rwlock_init_for_glibc@Base 0.9.8
+ (arch=musl-any-any)libunistring_glthread_rwlock_init_multithreaded@Base 1.0-2
+ (arch=musl-any-any)libunistring_glthread_rwlock_rdlock_multithreaded@Base 1.0-2
+ (arch=musl-any-any)libunistring_glthread_rwlock_unlock_multithreaded@Base 1.0-2
+ (arch=musl-any-any)libunistring_glthread_rwlock_wrlock_multithreaded@Base 1.0-2
  libunistring_hard_locale@Base 0.9.7
  libunistring_iconveh_close@Base 0.9.7
  libunistring_iconveh_open@Base 0.9.7
EOF
}

add_automatic libusb
add_automatic libusb-1.0
add_automatic libverto

add_automatic libx11
buildenv_libx11() {
	export xorg_cv_malloc0_returns_null=no
}

add_automatic libxau
add_automatic libxaw
add_automatic libxcb

add_automatic libxcrypt
patch_libxcrypt() {
	echo "do not abuse Important and Protected fields #1024616"
	drop_privs sed -i -e '/\(Important\|Protected\):/d' debian/control
}

add_automatic libxdmcp

add_automatic libxext
buildenv_libxext() {
	export xorg_cv_malloc0_returns_null=no
}

add_automatic libxmu
add_automatic libxpm

add_automatic libxrender
buildenv_libxrender() {
	export xorg_cv_malloc0_returns_null=no
}

add_automatic libxss
buildenv_libxss() {
	export xorg_cv_malloc0_returns_null=no
}

add_automatic libxt
buildenv_libxt() {
	export xorg_cv_malloc0_returns_null=no
}

add_automatic libzstd
patch_libzstd() {
	# #1037088 Build fails on "nodoc":
	# cp: cannot stat '/tmp/buildd/libzstd/libzstd-1.5.4+dfsg2/debian/zstd/usr/share/man/man1/zstd.1': No such file or directory
	drop_privs patch -p1 <<'EOF'
--- libzstd-1.5.4+dfsg2/debian/rules
+++ libzstd-1.5.4+dfsg2/debian/rules
@@ -62,10 +62,12 @@
 override_dh_makeshlibs:
 	dh_makeshlibs -plibzstd1 -V'libzstd1 (>= 1.5.2)' --add-udeb=libzstd1-udeb
 
+ifeq (,$(filter nodoc,$(DEB_BUILD_PROFILES)))
 execute_after_dh_installman:
 	cp $(mandir)/zstd.1 $(mandir)/zstdmt.1
 	$(HELP2MAN) --name='parallelized Zstandard compression, a la pigz' contrib/pzstd/pzstd \
 	| perl -pe 's/(\(de\)compression\s\(default:)(\d+)(\))/$$1 All$$3/g' >$(mandir)/pzstd.1
+endif
 
 build:
 	dh $@
EOF
}

patch_linux() {
	local kernel_arch comment regen_control
	kernel_arch=
	comment="just building headers yet"
	regen_control=
	case "$HOST_ARCH" in
		i586)
			# Vortex: Use headers from standard
			kernel_arch=i386
		;;
		arc|csky|ia64|nios2)
			kernel_arch=$HOST_ARCH
		;;
		loong64) kernel_arch=loongarch ;;
		mipsr6|mipsr6el|mipsn32r6|mipsn32r6el|mips64r6|mips64r6el)
			kernel_arch=defines-only
		;;
		powerpcel) kernel_arch=powerpc; ;;
		riscv64) kernel_arch=riscv; ;;
		*-linux-*)
			if ! test -d "debian/config/$HOST_ARCH"; then
				kernel_arch=$(sed 's/^kernel-arch: //;t;d' < "debian/config/${HOST_ARCH#*-linux-}/defines")
				comment="$HOST_ARCH must be part of a multiarch installation with a ${HOST_ARCH#*-linux-*} kernel"
			fi
		;;
	esac
	if test -n "$kernel_arch"; then
		if test "$kernel_arch" != defines-only; then
			echo "patching linux for $HOST_ARCH with kernel-arch $kernel_arch"
			drop_privs mkdir -p "debian/config/$HOST_ARCH"
			drop_privs tee "debian/config/$HOST_ARCH/defines" >/dev/null <<EOF
[base]
kernel-arch: $kernel_arch
featuresets:
# empty; $comment
EOF
		else
			echo "patching linux to enable $HOST_ARCH"
		fi
		drop_privs sed -i -e "/^arches:/a\\ $HOST_ARCH" debian/config/defines
		regen_control=yes
	fi
	test "$regen_control" = yes || return 0
	apt_get_install kernel-wedge python3-jinja2
	drop_privs ./debian/rules debian/rules.gen || : # intentionally exits 1 to avoid being called automatically. we are doing it wrong
}

add_automatic lz4
add_automatic m4
add_automatic make-dfsg
add_automatic man-db
add_automatic mawk
add_automatic mpclib3
add_automatic mpfr4
patch_mpfr4() {
	# Add Arch i586
	drop_privs patch -p1 <<'EOF'
--- a/debian/libmpfr6.symbols
+++ b/debian/libmpfr6.symbols
@@ -430,7 +430,7 @@
  mpfr_clear_overflow@Base 3.1.3
  mpfr_clear_underflow@Base 3.1.3
  mpfr_clears@Base 3.1.3
- (arch=!any-amd64 !any-i386 !x32 !any-arm64 !armel !eabihf-any-any-arm !hppa !ia64 !m68k)mpfr_clz_tab@Base 4.0.0
+ (arch=!any-amd64 !any-i386 !any-i586 !x32 !any-arm64 !armel !eabihf-any-any-arm !hppa !ia64 !m68k)mpfr_clz_tab@Base 4.0.0
  mpfr_cmp2@Base 3.1.3
  mpfr_cmp3@Base 3.1.3
  mpfr_cmp@Base 3.1.3
EOF
}

add_automatic mtools # by syslinux

builddep_ncurses() {
	if test "$(dpkg-architecture "-a$HOST_ARCH" -qDEB_HOST_ARCH_OS)" = linux; then
		assert_built gpm
		apt_get_install "libgpm-dev:$1"
	fi
	# g++-multilib dependency unsatisfiable
	apt_get_install debhelper pkg-config autoconf-dickey
	case "$ENABLE_MULTILIB:$HOST_ARCH" in
		yes:amd64|yes:i386|yes:powerpc|yes:ppc64|yes:s390|yes:sparc)
			test "$1" = "$HOST_ARCH"
			apt_get_install "g++-$GCC_VER-multilib$HOST_ARCH_SUFFIX"
			# the unversioned gcc-multilib$HOST_ARCH_SUFFIX should contain the following link
			ln -sf "`dpkg-architecture -a$HOST_ARCH -qDEB_HOST_MULTIARCH`/asm" /usr/include/asm
		;;
	esac
}

add_automatic nettle
add_automatic nghttp2
add_automatic npth
add_automatic nspr
patch_nspr() {
	# Add Arch i586
	drop_privs patch -p1 <<'EOF'
--- a/debian/libnspr4.symbols
+++ a/debian/libnspr4.symbols
@@ -408,10 +408,10 @@
  (arch=amd64 kfreebsd-amd64 x32)_PR_x86_64_AtomicDecrement@Base 2:4.9-2~
  (arch=amd64 kfreebsd-amd64 x32)_PR_x86_64_AtomicIncrement@Base 2:4.9-2~
  (arch=amd64 kfreebsd-amd64 x32)_PR_x86_64_AtomicSet@Base 2:4.9-2~
- (arch=i386 kfreebsd-i386 hurd-i386)_PR_x86_AtomicAdd@Base 2:4.9-2~
- (arch=i386 kfreebsd-i386 hurd-i386)_PR_x86_AtomicDecrement@Base 2:4.9-2~
- (arch=i386 kfreebsd-i386 hurd-i386)_PR_x86_AtomicIncrement@Base 2:4.9-2~
- (arch=i386 kfreebsd-i386 hurd-i386)_PR_x86_AtomicSet@Base 2:4.9-2~
+ (arch=i386 arch=i586 kfreebsd-i386 hurd-i386)_PR_x86_AtomicAdd@Base 2:4.9-2~
+ (arch=i386 arch=i586 kfreebsd-i386 hurd-i386)_PR_x86_AtomicDecrement@Base 2:4.9-2~
+ (arch=i386 arch=i586 kfreebsd-i386 hurd-i386)_PR_x86_AtomicIncrement@Base 2:4.9-2~
+ (arch=i386 arch=i586 kfreebsd-i386 hurd-i386)_PR_x86_AtomicSet@Base 2:4.9-2~
  _pr_push_ipv6toipv4_layer@Base 2:4.9-2~
  _pr_test_ipv6_socket@Base 2:4.9-2~
  libVersionPoint@Base 2:4.9-2~
EOF
}

add_automatic nss
patch_nss() {
	if dpkg-architecture "-a$HOST_ARCH" -iany-ppc64el; then
		echo "fix FTCBFS for ppc64el #948523"
		drop_privs patch -p1 <<'EOF'
--- a/debian/rules
+++ b/debian/rules
@@ -40,7 +40,8 @@
 ifeq ($(origin RANLIB),default)
 TOOLCHAIN += RANLIB=$(DEB_HOST_GNU_TYPE)-ranlib
 endif
-TOOLCHAIN += OS_TEST=$(DEB_HOST_GNU_CPU)
+OS_TYPE_map_powerpc64le = ppc64le
+TOOLCHAIN += OS_TEST=$(or $(OS_TYPE_map_$(DEB_HOST_GNU_CPU)),$(DEB_HOST_GNU_CPU))
 TOOLCHAIN += KERNEL=$(DEB_HOST_ARCH_OS)
 endif

EOF
	fi
	echo "work around FTBFS #984258"
	drop_privs patch -p1 <<'EOF'
--- a/debian/rules
+++ b/debian/rules
@@ -110,6 +110,7 @@
 		NSPR_LIB_DIR=/usr/lib/$(DEB_HOST_MULTIARCH) \
 		BUILD_OPT=1 \
 		NS_USE_GCC=1 \
+		NSS_ENABLE_WERROR=0 \
 		OPTIMIZER="$(CFLAGS) $(CPPFLAGS)" \
 		LDFLAGS='$(LDFLAGS) $$(ARCHFLAG) $$(ZDEFS_FLAG)' \
 		DSO_LDOPTS='-shared $$(LDFLAGS)' \
EOF
}

buildenv_openldap() {
	export ol_cv_pthread_select_yields=yes
	export ac_cv_func_memcmp_working=yes
}

add_automatic openssl
add_automatic openssl1.0

add_automatic p11-kit
patch_p11_kit() {
	dpkg-architecture "-a$HOST_ARCH" -ihurd-any || return 0
	echo "addressing FTBFS on hurd-any #989235"
	drop_privs patch -p1 <<'EOF'
--- a/p11-kit/lists.c
+++ b/p11-kit/lists.c
@@ -40,6 +40,7 @@
 #include <assert.h>
 #include <ctype.h>
 #include <string.h>
+#include <stdint.h>
 #include <stdio.h>
 #include <stdlib.h>
 #include <unistd.h>
EOF
}
patch_openssl() {
	if test "$ENABLE_MULTIARCH_GCC" != yes; then
		drop_privs patch -p1 <<'EOF'
--- openssl-3.0.8-old/debian/rules
+++ openssl-3.0.8-new/debian/rules
@@ -134,6 +134,10 @@
 	mkdir -p debian/tmp/usr/include/$(DEB_HOST_MULTIARCH)/openssl
 	mv debian/tmp/usr/include/openssl/opensslconf.h debian/tmp/usr/include/$(DEB_HOST_MULTIARCH)/openssl/
 	mv debian/tmp/usr/include/openssl/configuration.h debian/tmp/usr/include/$(DEB_HOST_MULTIARCH)/openssl/
+ifneq ($(ENABLE_MULTIARCH_GCC),yes)
+	mkdir -p debian/tmp/usr/$(DEB_HOST_MULTIARCH)/include
+	ln -s /usr/include/$(DEB_HOST_MULTIARCH)/openssl debian/tmp/usr/$(DEB_HOST_MULTIARCH)/include/openssl
+endif
 
 override_dh_installchangelogs:
 	dh_installchangelogs CHANGES.md
--- a/debian/libssl-dev.install
+++ b/debian/libssl-dev.install
@@ -5,3 +5,4 @@
 usr/lib/*/pkgconfig
 usr/include/openssl/*
 usr/include/*/openssl
+usr/*/include/openssl
EOF
	fi
	# Add debian Arch i586
	cat <<'EOF' >Configurations/21-debian-i586.conf
my %targets = (
	"debian-i586" => {
		inherit_from => [ "linux-elf", "debian" ],
	},
);
EOF
}

add_automatic patch
add_automatic pcre2
add_automatic pcre3

buildenv_perl() {
	buildtype="any,all"
	#profiles="$profiles,nodoc"
}
patch_perl() {
	echo "add missing libcrypt-dev dependency #1029753"
	drop_privs sed -i -e '/^Build-Depends:/s/$/libcrypt-dev,/' debian/control

	# Build fails on profile "nodoc" #50091
	# mv: cannot stat 'debian/libperl5.36/usr/share/man/man1/perl.1': No such file or directory
	drop_privs patch -p1 <<'EOF'
--- perl-5.36.0/debian/rules
+++ perl-5.36.0/debian/rules
@@ -287,11 +287,13 @@
 endif
 
 execute_after_dh_installman-arch:
+ifeq (,$(filter nodoc, $(DEB_BUILD_PROFILES)))
 	# put version+arch -specific manpages in libperl5.xx
 	for prefix in perl cpan; do \
 	  mv debian/libperl$(version)/usr/share/man/man1/$$prefix.1 \
 	     debian/libperl$(version)/usr/share/man/man1/$${prefix}$(version)-$(archtriplet).1; \
 	done
+endif
 
 override_dh_strip:
 	dh_strip --no-package=perl-debug --dbg-package=perl-debug
EOF

	# Add arch i586 as copy from i386
	drop_privs mkdir debian/cross/i586
	drop_privs cp debian/cross/i386/* debian/cross/i586/

	local NAME
	for NAME in debian/cross/i586/* ; do
		drop_privs sed -e 's/i686-linux/i586-linux/g' \
		    -e 's/i386-linux-gnu/i586-linux-gnu/g' \
		    -e 's/i686 gnulinux/i586 gnulinux/g' \
		    -i $NAME
	done
}

add_automatic pkgconf
add_automatic popt
#add_automatic python3-defaults
builddep_python3_defaults() {
	apt_get_build_dep "-a$HOST_ARCH" --arch-only "-P$2" ./ || :
	apt_get_install python3-sphinx html2text
}
#add_automatic python3.11
builddep_python3_11() {
	apt_get_build_dep "-a$HOST_ARCH" --arch-only "-P$2" ./ || :
	# Missing openssl/opensslconf.h
	#apt_get_remove libssl-dev
	# Missing sys/sdt.h
	apt_get_build_dep "-a$HOST_ARCH" "-P$2" ./ || :
	apt_get_build_dep "-P$2" ./
	apt_get_remove libssl-dev tcl8.6-dev
	apt_get_install systemtap-sdt-dev:$HOST_ARCH libssl-dev:$HOST_ARCH
	# conftest.c:174:10: fatal error: gdbm/ndbm.h: No such file or directory
	apt_get_install libgdbm-compat-dev:$HOST_ARCH tcl8.6-dev:$HOST_ARCH libffi-dev:$HOST_ARCH libexpat1-dev:$HOST_ARCH
}
patch_python3_11() {
	# Add arch i586
	drop_privs patch -p1 <<'EOF'
--- python3.11-3.11.2/debian/rules
+++ python3.11-3.11.2/debian/rules
@@ -204,13 +204,13 @@
 
 ifeq ($(DEB_HOST_GNU_TYPE),$(DEB_BUILD_GNU_TYPE))
   ifeq ($(DEB_HOST_ARCH_OS),linux)
-    ifneq (,$(findstring $(DEB_HOST_ARCH), amd64 armel armhf arm64 i386 powerpc ppc64 ppc64el s390x))
+    ifneq (,$(findstring $(DEB_HOST_ARCH), amd64 armel armhf arm64 i386 i586 powerpc ppc64 ppc64el s390x))
       with_pgo := yes
     endif
   endif
 endif
 
-ifneq (,$(filter $(DEB_HOST_ARCH), amd64 armel armhf arm64 i386 powerpc ppc64 ppc64el s390x))
+ifneq (,$(filter $(DEB_HOST_ARCH), amd64 armel armhf arm64 i386 i586 powerpc ppc64 ppc64el s390x))
   with_lto := yes
 endif
 
--- python3.11-3.11.2/configure.ac
+++ python3.11-3.11.2/configure.ac
@@ -936,6 +936,8 @@
         x86_64-linux-gnu
 # elif defined(__x86_64__) && defined(__ILP32__)
         x86_64-linux-gnux32
+# elif defined(__i586__)
+        i586-linux-gnu
 # elif defined(__i386__)
         i386-linux-gnu
 # elif defined(__aarch64__) && defined(__AARCH64EL__)
--- python3.11-3.11.2/debian/multiarch.h.in
+++ python3.11-3.11.2/debian/multiarch.h.in
@@ -3,6 +3,8 @@
 #  include <x86_64-linux-gnu/@subdir@/@header@>
 # elif defined(__x86_64__) && defined(__ILP32__)
 #  include <x86_64-linux-gnux32/@subdir@/@header@>
+# elif defined(__i586__)
+#  include <i586-linux-gnu/@subdir@/@header@>
 # elif defined(__i386__)
 #  include <i386-linux-gnu/@subdir@/@header@>
 # elif defined(__aarch64__) && defined(__AARCH64EL__)
EOF
}

builddep_readline() {
	assert_built "ncurses"
	# gcc-multilib dependency unsatisfiable
	apt_get_install debhelper "libtinfo-dev:$1" "libncursesw5-dev:$1" mawk texinfo autotools-dev
	case "$ENABLE_MULTILIB:$HOST_ARCH" in
		yes:amd64|yes:ppc64)
			test "$1" = "$HOST_ARCH"
			apt_get_install "gcc-$GCC_VER-multilib$HOST_ARCH_SUFFIX" "lib32tinfo-dev:$1" "lib32ncursesw5-dev:$1"
			# the unversioned gcc-multilib$HOST_ARCH_SUFFIX should contain the following link
			ln -sf "`dpkg-architecture -a$1 -qDEB_HOST_MULTIARCH`/asm" /usr/include/asm
		;;
		yes:i386|yes:powerpc|yes:sparc|yes:s390)
			test "$1" = "$HOST_ARCH"
			apt_get_install "gcc-$GCC_VER-multilib$HOST_ARCH_SUFFIX" "lib64ncurses5-dev:$1"
			# the unversioned gcc-multilib$HOST_ARCH_SUFFIX should contain the following link
			ln -sf "`dpkg-architecture -a$1 -qDEB_HOST_MULTIARCH`/asm" /usr/include/asm
		;;
	esac
}

add_automatic reprepro
patch_reprepro() {
	drop_privs sed 's/libgpgme-dev, //g' -i debian/control
	drop_privs sed 's/--with-libgpgme/--without-libgpgme/g' -i debian/rules
}
add_automatic rtmpdump

add_automatic sed
patch_sed() {
	dpkg-architecture "-a$HOST_ARCH" -imusl-any-any || return 0
	echo "musl FTBFS #1010224"
	drop_privs sed -i -e '1ainclude /usr/share/dpkg/architecture.mk' debian/rules
	drop_privs sed -i -e 's/--without-included-regex/--with$(if $(filter musl,$(DEB_HOST_ARCH_LIBC)),,out)-included-regex/' debian/rules
}

add_automatic shadow
add_automatic slang2
add_automatic spdylay
add_automatic sqlite3
add_automatic syslinux
builddep_syslinux() {
	# nasm:i586 python3:i586 would be wrong here. e2fslibs-dev is missing, util-linux from build_arch
	# uuid-dev need for BUILD-Host
	apt_get_install nasm e2fslibs-dev:$HOST_ARCH python3 uuid-dev
}
patch_syslinux() {
	if [ "$HOST_ARCH" = "i586" ]; then
		echo "syslinux must build as i386"
		drop_privs patch -p1 <<'EOF'
--- a/debian/rules
+++ b/debian/rules
@@ -8,6 +8,12 @@
 # required in patched mk/efi.mk makefile to link against the gnu-efi package
 export DEB_HOST_ARCH ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)
 
+ifeq ($(DEB_HOST_ARCH),i586)
+# Force cross compile (a marker only)
+export DEB_HOST_ARCH = i386
+export COSSCOMPILE = $(shell dpkg-architecture -qDEB_TARGET_GNU_TYPE)-
+endif
+
 unexport LDFLAGS
 
 %:
@@ -63,6 +69,18 @@
 override_dh_installman:
 	dh_installman --language=C
 
+ifneq (,$(COSSCOMPILE))
+override_dh_shlibdeps:
+ifeq ($(ENABLE_MULTIARCH_GCC), yes)
+	dh_shlibdeps -a -l/usr/lib/i586-linux-gnu
+else
+	dh_shlibdeps -a -l/usr/i586-linux-gnu/lib
+endif
+
+override_dh_gencontrol:
+	dh_gencontrol -a -- -DArchitecture=i586
+endif
+
 override_dh_gencontrol-indep:
 	dh_gencontrol -- -Vbuilt:Using="$(shell dpkg-query -f '$${source:Package} (= $${source:Version}), ' -W gnu-efi)"
 
EOF
	fi
}
add_automatic systemtap
builddep_systemtap() {
	apt_get_build_dep "-a$HOST_ARCH" --arch-only "-P$2" ./ || :
	apt_get_build_dep "-P$2" ./
	apt_get_install libsqlite3-dev:$HOST_ARCH libelf-dev:$HOST_ARCH libdw-dev:$HOST_ARCH libnss3-dev:$HOST_ARCH
}
add_automatic sysvinit

add_automatic tar
buildenv_tar() {
	case $(dpkg-architecture "-a$HOST_ARCH" -qDEB_HOST_GNU_SYSTEM) in *gnu*)
		echo "struct dirent contains working d_ino on glibc systems"
		export gl_cv_struct_dirent_d_ino=yes
	;; esac
	if ! dpkg-architecture "-a$HOST_ARCH" -ilinux-any; then
		echo "forcing broken posix acl check to fail on non-linux #850668"
		export gl_cv_getxattr_with_posix_acls=no
	fi
	# Allow configure tar as root #1033748
	if test -z "$DROP_PRIVS"; then
		export FORCE_UNSAFE_CONFIGURE=1
	fi
}

add_automatic tcl8.6
buildenv_tcl8_6() {
	export tcl_cv_strtod_buggy=ok
	export tcl_cv_strtoul_unbroken=ok
}

add_automatic tcltk-defaults
add_automatic tcp-wrappers
add_automatic texinfo

add_automatic tk8.6
buildenv_tk8_6() {
	export tcl_cv_strtod_buggy=ok
}
add_automatic tzdata
buildenv_tzdata() {
	buildtype="any,all"
}
builddep_tzdata() {
	# Python from host, not from cross
	apt_get_install gawk po-debconf python3 python3-debian python3-natsort python3-polib
}

add_automatic uchardet
add_automatic ustr

buildenv_util_linux() {
	export scanf_cv_type_modifier=ms
}

add_automatic xft
add_automatic xxhash

add_automatic xz-utils
buildenv_xz_utils() {
	if dpkg-architecture "-a$1" -imusl-linux-any; then
		echo "ignoring symbol differences for musl for now"
		export DPKG_GENSYMBOLS_CHECK_LEVEL=0
	fi
}

builddep_zlib() {
	# gcc-multilib dependency unsatisfiable
	apt_get_install debhelper binutils dpkg-dev
}

# choosing libatomic1 arbitrarily here, cause it never bumped soname
BUILD_GCC_MULTIARCH_VER=`apt-cache show --no-all-versions libatomic1 | sed 's/^Source: gcc-\([0-9.]*\)$/\1/;t;d'`
if test "$GCC_VER" != "$BUILD_GCC_MULTIARCH_VER"; then
	echo "host gcc version ($GCC_VER) and build gcc version ($BUILD_GCC_MULTIARCH_VER) mismatch. need different build gcc"
	if dpkg --compare-versions "$GCC_VER" gt "$BUILD_GCC_MULTIARCH_VER"; then
		echo "deb [ arch=$(dpkg --print-architecture) ] $MIRROR experimental main" > /etc/apt/sources.list.d/tmp-experimental.list
		$APT_GET update
		$APT_GET -t experimental install g++ g++-$GCC_VER
		rm -f /etc/apt/sources.list.d/tmp-experimental.list
		$APT_GET update
	elif test -f "$REPODIR/stamps/gcc_0"; then
		echo "skipping rebuild of build gcc"
		$APT_GET --force-yes dist-upgrade # downgrade!
	else
		cross_build_setup "gcc-$GCC_VER" gcc0
		apt_get_build_dep --arch-only ./
		# dependencies for common libs no longer declared
		apt_get_install doxygen graphviz ghostscript texlive-latex-base xsltproc docbook-xsl-ns
		(
			export gcc_cv_libc_provides_ssp=yes
			nolang=$(set_add "${GCC_NOLANG:-}" biarch)
			export DEB_BUILD_OPTIONS="$DEB_BUILD_OPTIONS nostrap nolang=$(join_words , $nolang)"
			drop_privs_exec dpkg-buildpackage -B -uc -us
		)
		cd ..
		ls -l
		reprepro include rebootstrap-native ./*.changes
		drop_privs rm -fv ./*-plugin-dev_*.deb ./*-dbg_*.deb
		dpkg -i *.deb
		touch "$REPODIR/stamps/gcc_0"
		cd ..
		drop_privs rm -Rf gcc0
	fi
	progress_mark "build compiler complete"
else
	echo "host gcc version and build gcc version match. good for multiarch"
fi

if test -f "$REPODIR/stamps/cross-binutils"; then
	echo "skipping rebuild of binutils-target"
else
	cross_build_setup binutils
	check_binNMU
	apt_get_build_dep --arch-only -Pnocheck ./
	drop_privs TARGET=$HOST_ARCH dpkg-buildpackage -B -Pnocheck --target=stamps/control
	drop_privs TARGET=$HOST_ARCH dpkg-buildpackage -B -uc -us -Pnocheck
	cd ..
	ls -l
	pickup_packages *.changes
	apt_get_install "binutils$HOST_ARCH_SUFFIX"
	assembler="`dpkg-architecture -a$HOST_ARCH -qDEB_HOST_GNU_TYPE`-as"
	if ! command -v "$assembler" >/dev/null; then echo "$assembler missing in binutils package"; exit 1; fi
	if ! drop_privs "$assembler" -o test.o /dev/null; then echo "binutils fail to execute"; exit 1; fi
	if ! test -f test.o; then echo "binutils fail to create object"; exit 1; fi
	check_arch test.o "$HOST_ARCH"
	touch "$REPODIR/stamps/cross-binutils"
	cd ..
	drop_privs rm -Rf binutils
fi
progress_mark "cross binutils"

if test "$HOST_ARCH" = hppa && ! test -f "$REPODIR/stamps/cross-binutils-hppa64"; then
	cross_build_setup binutils binutils-hppa64
	check_binNMU
	apt_get_build_dep --arch-only -Pnocheck ./
	drop_privs with_hppa64=yes DEB_BUILD_OPTIONS="$DEB_BUILD_OPTIONS nocross nomult nopgo" dpkg-buildpackage -B -Pnocheck --target=stamps/control
	drop_privs with_hppa64=yes DEB_BUILD_OPTIONS="$DEB_BUILD_OPTIONS nocross nomult nopgo" dpkg-buildpackage -B -uc -us -Pnocheck
	cd ..
	ls -l
	pickup_additional_packages binutils-hppa64-linux-gnu_*.deb
	apt_get_install binutils-hppa64-linux-gnu
	if ! command -v hppa64-linux-gnu-as >/dev/null; then echo "hppa64-linux-gnu-as missing in binutils package"; exit 1; fi
	if ! drop_privs hppa64-linux-gnu-as -o test.o /dev/null; then echo "binutils-hppa64 fail to execute"; exit 1; fi
	if ! test -f test.o; then echo "binutils-hppa64 fail to create object"; exit 1; fi
	check_arch test.o hppa64
	touch "$REPODIR/stamps/cross-binutils-hppa64"
	cd ..
	drop_privs rm -Rf binutils-hppa64-linux-gnu
	progress_mark "cross binutils-hppa64"
fi

if test "`dpkg-architecture "-a$HOST_ARCH" -qDEB_HOST_ARCH_OS`" = "linux"; then
	if test -f "$REPODIR/stamps/linux_1"; then
		echo "skipping rebuild of linux-libc-dev"
	else
		cross_build_setup linux
		check_binNMU
		if dpkg-architecture -ilinux-any && test "$(dpkg-query -W -f '${Version}' "linux-libc-dev:$(dpkg --print-architecture)")" != "$(dpkg-parsechangelog -SVersion)"; then
			echo "rebootstrap-warning: working around linux-libc-dev m-a:same skew"
			apt_get_build_dep --arch-only -Pstage1 ./
			drop_privs KBUILD_VERBOSE=1 dpkg-buildpackage -B -Pstage1 -uc -us
		fi
		apt_get_build_dep --arch-only "-a$HOST_ARCH" -Pstage1 ./
		drop_privs KBUILD_VERBOSE=1 dpkg-buildpackage -B "-a$HOST_ARCH" -Pstage1 -uc -us
		cd ..
		ls -l
		if test "$ENABLE_MULTIARCH_GCC" != yes; then
			drop_privs dpkg-cross -M -a "$HOST_ARCH" -b ./*"_$HOST_ARCH.deb"
		fi
		pickup_packages *.deb
		touch "$REPODIR/stamps/linux_1"
		compare_native ./*.deb
		cd ..
		drop_privs rm -Rf linux
	fi
	progress_mark "linux-libc-dev cross build"
fi

if test "$(dpkg-architecture "-a$HOST_ARCH" -qDEB_HOST_ARCH_OS)" = hurd; then
	if test -f "$REPODIR/stamps/gnumach_1"; then
		echo "skipping rebuild of gnumach stage1"
	else
		cross_build_setup gnumach gnumach_1
		apt_get_build_dep "-a$HOST_ARCH" --arch-only -Pstage1 ./
		drop_privs dpkg-buildpackage -B "-a$HOST_ARCH" -Pstage1 -uc -us
		cd ..
		pickup_packages ./*.deb
		touch "$REPODIR/stamps/gnumach_1"
		cd ..
		drop_privs rm -Rf gnumach_1
	fi
	progress_mark "gnumach stage1 cross build"
fi

GCC_AUTOCONF=autoconf2.69

if test -f "$REPODIR/stamps/gcc_1"; then
	echo "skipping rebuild of gcc stage1"
else
	apt_get_install debhelper gawk patchutils bison flex lsb-release quilt libtool $GCC_AUTOCONF zlib1g-dev libmpc-dev libmpfr-dev libgmp-dev systemtap-sdt-dev sharutils "binutils$HOST_ARCH_SUFFIX" time
	if test "$(dpkg-architecture "-a$HOST_ARCH" -qDEB_HOST_ARCH_OS)" = linux; then
		if test "$ENABLE_MULTIARCH_GCC" = yes; then
			apt_get_install "linux-libc-dev:$HOST_ARCH"
		else
			apt_get_install "linux-libc-dev-${HOST_ARCH}-cross"
		fi
	fi
	if test "$HOST_ARCH" = hppa; then
		apt_get_install binutils-hppa64-linux-gnu
	fi
	cross_build_setup "gcc-$GCC_VER" gcc1
	check_binNMU
	dpkg-checkbuilddeps || : # tell unmet build depends
	echo "$HOST_ARCH" > debian/target
	(
		nolang=${GCC_NOLANG:-}
		test "$ENABLE_MULTILIB" = yes || nolang=$(set_add "$nolang" biarch)
		export DEB_STAGE=stage1
		export DEB_BUILD_OPTIONS="$DEB_BUILD_OPTIONS${nolang:+ nolang=$(join_words , $nolang)}"
		drop_privs dpkg-buildpackage -d -T control
		dpkg-checkbuilddeps || : # tell unmet build depends again after rewriting control
		drop_privs_exec dpkg-buildpackage -d --build=$BUILD_TYPE -uc -us
	)
	cd ..
	ls -l
	pickup_packages *.changes
	apt_get_remove gcc-multilib
	if test "$ENABLE_MULTILIB" = yes && ls | grep -q multilib; then
		apt_get_install "gcc-$GCC_VER-multilib$HOST_ARCH_SUFFIX"
	else
		rm -vf ./*multilib*.deb
		# Vortex devel: Remove the old, if exist
		apt_get_remove "gcc-$GCC_VER$HOST_ARCH_SUFFIX"
		apt_get_remove "gcc-$GCC_VER$HOST_ARCH_SUFFIX-base"
		apt_get_install "gcc-$GCC_VER$HOST_ARCH_SUFFIX"
	fi
	compiler="`dpkg-architecture "-a$HOST_ARCH" -qDEB_HOST_GNU_TYPE`-gcc-$GCC_VER"
	if ! command -v "$compiler" >/dev/null; then echo "$compiler missing in stage1 gcc package"; exit 1; fi
	if ! drop_privs "$compiler" -x c -c /dev/null -o test.o; then echo "stage1 gcc fails to execute"; exit 1; fi
	if ! test -f test.o; then echo "stage1 gcc fails to create binaries"; exit 1; fi
	check_arch test.o "$HOST_ARCH"
	touch "$REPODIR/stamps/gcc_1"
	cd ..
	drop_privs rm -Rf gcc1
fi
progress_mark "cross gcc stage1 build"

# replacement for cross-gcc-defaults
for prog in c++ cpp g++ gcc gcc-ar gcc-ranlib gfortran; do
	ln -fs "`dpkg-architecture "-a$HOST_ARCH" -qDEB_HOST_GNU_TYPE`-$prog-$GCC_VER" "/usr/bin/`dpkg-architecture "-a$HOST_ARCH" -qDEB_HOST_GNU_TYPE`-$prog"
done

if test "$(dpkg-architecture "-a$HOST_ARCH" -qDEB_HOST_ARCH_OS)" = hurd; then
	if test -f "$REPODIR/stamps/hurd_1"; then
		echo "skipping rebuild of hurd stage1"
	else
		cross_build_setup hurd hurd_1
		apt_get_build_dep "-a$HOST_ARCH" --arch-only -P stage1 ./
		drop_privs dpkg-buildpackage -B "-a$HOST_ARCH" -Pstage1 -uc -us
		cd ..
		ls -l
		pickup_packages *.changes
		touch "$REPODIR/stamps/hurd_1"
		cd ..
		drop_privs rm -Rf hurd_1
	fi
	progress_mark "hurd stage1 cross build"
fi

if test "$(dpkg-architecture "-a$HOST_ARCH" -qDEB_HOST_ARCH_OS)" = hurd; then
	if test -f "$REPODIR/stamps/mig_1"; then
		echo "skipping rebuild of mig cross"
	else
		cross_build_setup mig mig_1
		apt_get_install dpkg-dev debhelper dh-exec dh-autoreconf "gnumach-dev:$HOST_ARCH" flex libfl-dev bison
		drop_privs dpkg-buildpackage -d -B "--target-arch=$HOST_ARCH" -uc -us
		cd ..
		ls -l
		pickup_packages *.changes
		touch "$REPODIR/stamps/mig_1"
		cd ..
		drop_privs rm -Rf mig_1
	fi
	progress_mark "cross mig build"
fi

# we'll have to remove build arch multilibs to be able to install host arch multilibs
apt_get_remove $(dpkg-query -W "libc[0-9]*-*:$(dpkg --print-architecture)" | sed "s/\\s.*//;/:$(dpkg --print-architecture)/d")

# Vortex: remove current packages
apt_get_remove $(dpkg-query -W "libc[0-9]*-*:$HOST_ARCH" | sed "s/\\s.*//;/:$(dpkg --print-architecture)/d")

case "$HOST_ARCH" in
	musl-linux-*) LIBC_NAME=musl ;;
	*) LIBC_NAME=glibc ;;
esac
if test -f "$REPODIR/stamps/${LIBC_NAME}_2"; then
	echo "skipping rebuild of $LIBC_NAME stage2"
else
	cross_build_setup "$LIBC_NAME" "${LIBC_NAME}_2"
	if dpkg-architecture "-a$HOST_ARCH" -ignu-any-any; then
		"$(get_hook builddep glibc)" "$HOST_ARCH" stage2
	else
		apt_get_build_dep "-a$HOST_ARCH" --arch-only ./
	fi
	(
		profiles=$(join_words , $DEFAULT_PROFILES)
		if dpkg-architecture "-a$HOST_ARCH" -ignu-any-any; then
			profiles="$profiles,stage2"
			test "$ENABLE_MULTILIB" != yes && profiles="$profiles,nobiarch"
			buildenv_glibc
		fi
		# tell unmet build depends
		drop_privs dpkg-checkbuilddeps -B "-a$HOST_ARCH" "-P$profiles" || :
		drop_privs_exec dpkg-buildpackage -B -uc -us "-a$HOST_ARCH" -d "-P$profiles" || buildpackage_failed "$?"
	)
	cd ..
	ls -l
	if dpkg-architecture "-a$HOST_ARCH" -imusl-any-any; then
		pickup_packages *.changes
		dpkg -i musl*.deb
	else
		if test "$ENABLE_MULTIARCH_GCC" = yes; then
			pickup_packages *.changes
			dpkg -i libc[0-9]*.deb
		else
			for pkg in libc[0-9]*.deb; do
				# dpkg-cross cannot handle these
				test "${pkg%%_*}" = "libc6-xen" && continue
				test "${pkg%%_*}" = "libc6.1-alphaev67" && continue
				drop_privs dpkg-cross -M -a "$HOST_ARCH" -X tzdata -X libc-bin -X libc-dev-bin -X multiarch-support -b "$pkg"
			done
			pickup_packages *.changes ./*-cross_*.deb
			dpkg -i libc[0-9]*-cross_*.deb
		fi
	fi
	# Stamp "glibc_2 or musl_2" here
	touch "$REPODIR/stamps/${LIBC_NAME}_2"
	compare_native ./*.deb
	cd ..
	drop_privs rm -Rf "${LIBC_NAME}_2"
fi
progress_mark "$LIBC_NAME stage2 cross build"

if test -f "$REPODIR/stamps/gcc_3"; then
	echo "skipping rebuild of gcc stage3"
else
	apt_get_install debhelper gawk patchutils bison flex lsb-release quilt libtool $GCC_AUTOCONF zlib1g-dev libmpc-dev libmpfr-dev libgmp-dev dejagnu systemtap-sdt-dev sharutils "binutils$HOST_ARCH_SUFFIX" time
	if test "$HOST_ARCH" = hppa; then
		apt_get_install binutils-hppa64-linux-gnu
	fi
	if test "$ENABLE_MULTIARCH_GCC" = yes; then
		apt_get_install "libc-dev:$HOST_ARCH" $(echo "$MULTILIB_NAMES" | sed "s/\(\S\+\)/libc6-dev-\1:$HOST_ARCH/g")
		# gcc_3 stops with missing isl/schedule.h, only with multiarch=yes
		apt_get_install libisl-dev
	else
		if dpkg-architecture "-a$HOST_ARCH" -ignu-any-any; then
			apt_get_install "libc6-dev-$HOST_ARCH-cross" $(echo "$MULTILIB_NAMES" | sed "s/\(\S\+\)/libc6-dev-\1-$HOST_ARCH-cross/g")
		elif dpkg-architecture "-a$HOST_ARCH" -imusl-any-any; then
			apt_get_install "musl-dev-$HOST_ARCH-cross"
		fi
	fi
	cross_build_setup "gcc-$GCC_VER" gcc3
	check_binNMU
	dpkg-checkbuilddeps -a$HOST_ARCH || : # tell unmet build depends
	echo "$HOST_ARCH" > debian/target
	(
		nolang=${GCC_NOLANG:-}
		test "$ENABLE_MULTILIB" = yes || nolang=$(set_add "$nolang" biarch)
		export DEB_BUILD_OPTIONS="$DEB_BUILD_OPTIONS${nolang:+ nolang=$(join_words , $nolang)}"
		if test "$ENABLE_MULTIARCH_GCC" = yes; then
			export with_deps_on_target_arch_pkgs=yes
		else
			export WITH_SYSROOT=/
		fi
		export gcc_cv_libc_provides_ssp=yes
		export gcc_cv_initfini_array=yes
		drop_privs dpkg-buildpackage -d -T control
		drop_privs dpkg-buildpackage -d -T clean
		dpkg-checkbuilddeps || : # tell unmet build depends again after rewriting control
		drop_privs_exec dpkg-buildpackage -d --build=$BUILD_TYPE -uc -us
	)
	cd ..
	ls -l
	if test "$ENABLE_MULTIARCH_GCC" = yes; then
		drop_privs changestool ./*.changes dumbremove "gcc-${GCC_VER}-base_"*"_$(dpkg --print-architecture).deb"
		drop_privs rm "gcc-${GCC_VER}-base_"*"_$(dpkg --print-architecture).deb"
	fi
	pickup_packages *.changes
	# avoid file conflicts between differently staged M-A:same packages
	apt_get_remove "gcc-$GCC_VER-base:$HOST_ARCH"
	drop_privs rm -fv gcc-*-plugin-*.deb gcj-*.deb gdc-*.deb ./*objc*.deb ./*-dbg_*.deb
	dpkg -i *.deb
	compiler="`dpkg-architecture -a$HOST_ARCH -qDEB_HOST_GNU_TYPE`-gcc-$GCC_VER"
	if ! command -v "$compiler" >/dev/null; then echo "$compiler missing in stage3 gcc package"; exit 1; fi
	if ! drop_privs "$compiler" -x c -c /dev/null -o test.o; then echo "stage3 gcc fails to execute"; exit 1; fi
	if ! test -f test.o; then echo "stage3 gcc fails to create binaries"; exit 1; fi
	check_arch test.o "$HOST_ARCH"
	triplet=`dpkg-architecture -a$HOST_ARCH -qDEB_HOST_MULTIARCH`
	if test "$ENABLE_MULTIARCH_GCC" = yes; then
		arch_include=/usr/include/$triplet
	else
		arch_include=/usr/$triplet/include
	fi
	mkdir -p $arch_include
	touch $arch_include/include_path_test_header.h
	preproc="`dpkg-architecture -a$HOST_ARCH -qDEB_HOST_GNU_TYPE`-cpp-$GCC_VER"
	if ! echo '#include "include_path_test_header.h"' | drop_privs "$preproc" -E -; then echo "stage3 gcc fails to search /usr/include/<triplet>"; exit 1; fi
	rm $arch_include/include_path_test_header.h
	touch "$REPODIR/stamps/gcc_3"
	if test "$ENABLE_MULTIARCH_GCC" = yes; then
		compare_native ./*.deb
	fi
	cd ..
	drop_privs rm -Rf gcc3
fi
progress_mark "cross gcc stage3 build"

if test "$ENABLE_MULTIARCH_GCC" != yes; then
	if test -f "$REPODIR/stamps/gcc_f1"; then
		echo "skipping rebuild of gcc rtlibs"
	else
		apt_get_install debhelper gawk patchutils bison flex lsb-release quilt libtool $GCC_AUTOCONF zlib1g-dev libmpc-dev libmpfr-dev libgmp-dev dejagnu systemtap-sdt-dev sharutils "binutils$HOST_ARCH_SUFFIX" "libc-dev:$HOST_ARCH" time
		if test "$HOST_ARCH" = hppa; then
			apt_get_install binutils-hppa64-linux-gnu
		fi
		if test "$ENABLE_MULTILIB" = yes -a -n "$MULTILIB_NAMES"; then
			apt_get_install $(echo "$MULTILIB_NAMES" | sed "s/\(\S\+\)/libc6-dev-\1-$HOST_ARCH-cross libc6-dev-\1:$HOST_ARCH/g")
		fi
		cross_build_setup "gcc-$GCC_VER" gcc_f1
		check_binNMU
		dpkg-checkbuilddeps || : # tell unmet build depends
		echo "$HOST_ARCH" > debian/target
		(
			export DEB_STAGE=rtlibs
			nolang=${GCC_NOLANG:-}
			test "$ENABLE_MULTILIB" = yes || nolang=$(set_add "$nolang" biarch)
			export DEB_BUILD_OPTIONS="$DEB_BUILD_OPTIONS${nolang:+ nolang=$(join_words , $nolang)}"
			export WITH_SYSROOT=/
			drop_privs dpkg-buildpackage -d -T control
			cat debian/control
			dpkg-checkbuilddeps || : # tell unmet build depends again after rewriting control
			drop_privs_exec dpkg-buildpackage -d --build=$BUILD_TYPE -uc -us
		)
		cd ..
		ls -l
		rm -vf "gcc-$GCC_VER-base_"*"_$(dpkg --print-architecture).deb"
		pickup_additional_packages *.deb
		$APT_GET dist-upgrade
		dpkg -i ./*.deb
		touch "$REPODIR/stamps/gcc_f1"
		cd ..
		drop_privs rm -Rf gcc_f1
	fi
	progress_mark "gcc cross rtlibs build"
fi

# install something similar to crossbuild-essential
apt_get_install "binutils$HOST_ARCH_SUFFIX" "gcc-$GCC_VER$HOST_ARCH_SUFFIX" "g++-$GCC_VER$HOST_ARCH_SUFFIX" "libc-dev:$HOST_ARCH"

apt_get_remove libc6-i386 # breaks cross builds

if dpkg-architecture "-a$HOST_ARCH" -ihurd-any; then
	if test -f "$REPODIR/stamps/hurd_2"; then
		echo "skipping rebuild of hurd stage2"
	else
		cross_build_setup hurd hurd_2
		apt_get_build_dep "-a$HOST_ARCH" --arch-only -P stage2 ./
		drop_privs dpkg-buildpackage -B "-a$HOST_ARCH" -Pstage2 -uc -us
		cd ..
		ls -l
		pickup_packages *.changes
		touch "$REPODIR/stamps/hurd_2"
		cd ..
		drop_privs rm -Rf hurd_2
	fi
	apt_get_install "hurd-dev:$HOST_ARCH"
	progress_mark "hurd stage3 cross build"
fi

apt_get_install dose-builddebcheck dctrl-tools

call_dose_builddebcheck() {
	local package_list source_list errcode
	package_list=`mktemp packages.XXXXXXXXXX`
	source_list=`mktemp sources.XXXXXXXXXX`
	cat /var/lib/apt/lists/*_Packages - > "$package_list" <<EOF
Package: crossbuild-essential-$HOST_ARCH
Version: 0
Architecture: $HOST_ARCH
Multi-Arch: foreign
Depends: libc-dev
Description: fake crossbuild-essential package for dose-builddebcheck

EOF
	sed -i -e '/^Conflicts:.* libc[0-9][^ ]*-dev\(,\|$\)/d' "$package_list" # also make dose ignore the glibc conflict
	apt-cache show "gcc-${GCC_VER}-base=installed" libgcc-s1=installed libstdc++6=installed libatomic1=installed >> "$package_list" # helps when pulling gcc from experimental
	cat /var/lib/apt/lists/*_Sources > "$source_list"
	errcode=0
	dose-builddebcheck --deb-tupletable=/usr/share/dpkg/tupletable --deb-cputable=/usr/share/dpkg/cputable "--deb-native-arch=$(dpkg --print-architecture)" "--deb-host-arch=$HOST_ARCH" "$@" "$package_list" "$source_list" || errcode=$?
	if test "$errcode" -gt 1; then
		echo "dose-builddebcheck failed with error code $errcode" 1>&2
		exit 1
	fi
	rm -f "$package_list" "$source_list"
}

# determine whether a given binary package refers to an arch:all package
# $1 is a binary package name
is_arch_all() {
	grep-dctrl -P -X "$1" -a -F Architecture all -s /var/lib/apt/lists/*_Packages
}

# determine which source packages build a given binary package
# $1 is a binary package name
# prints a set of source packages
what_builds() {
	local newline pattern source
	newline='
'
	pattern=`echo "$1" | sed 's/[+.]/\\\\&/g'`
	pattern="$newline $pattern "
	# exit codes 0 and 1 signal successful operation
	source=`grep-dctrl -F Package-List -e "$pattern" -s Package -n /var/lib/apt/lists/*_Sources || test "$?" -eq 1`
	set_create "$source"
}

# determine a set of source package names which are essential to some
# architecture
discover_essential() {
	set_create "$(grep-dctrl -F Package-List -e '\bessential=yes\b' -s Package -n /var/lib/apt/lists/*_Sources)"
}

need_packages=
add_need() { need_packages=`set_add "$need_packages" "$1"`; }
built_packages=
mark_built() {
	need_packages=`set_discard "$need_packages" "$1"`
	built_packages=`set_add "$built_packages" "$1"`
}

for pkg in $(discover_essential); do
	if set_contains "$automatic_packages" "$pkg"; then
		echo "rebootstrap-debug: automatically scheduling essential package $pkg"
		add_need "$pkg"
	else
		echo "rebootstrap-debug: not scheduling essential package $pkg"
	fi
done
add_need acl # by coreutils, systemd
add_need apt # almost essential
add_need attr # by coreutils, libcap-ng
add_need blt # by pythonX.Y
add_need bsdmainutils # for man-db
add_need bzip2 # by perl
add_need db-defaults # by perl, python3.X, reprepro
add_need dosfstools # by syslinux
add_need dwz # for debbuild
add_need expat # by unbound
add_need file # by gcc-6, for debhelper
add_need flex # by pam
add_need fribidi # by newt
add_need gcc-defaults # by build-essential
add_need gmp # by gnutls28
add_need gnupg2 # for apt
test "$(dpkg-architecture "-a$HOST_ARCH" -qDEB_HOST_ARCH_OS)" = linux && add_need gpm # by ncurses
add_need groff # for man-db
test "$(dpkg-architecture "-a$HOST_ARCH" -qDEB_HOST_ARCH_OS)" = linux && add_need kmod # by systemd
add_need icu # by libxml2
add_need krb5 # by audit
add_need libarchive # by reprepro
test "$HOST_ARCH" = ia64 && add_need libatomic-ops # by gcc-VER
add_need libbpf # by iproute2
dpkg-architecture "-a$HOST_ARCH" -ilinux-any && add_need libcap2 # by systemd
add_need libdebian-installer # by cdebconf
add_need libedit # by openssh
add_need libevent # by unbound
add_need libidn2 # by gnutls28
add_need libmnl # by iproute2
add_need libgcrypt20 # by libprelude, cryptsetup
dpkg-architecture "-a$HOST_ARCH" -ilinux-any && add_need libsepol # by libselinux
add_need libsigsegv # by m4
if dpkg-architecture "-a$HOST_ARCH" -ihurd-any; then
	add_need libsystemd-dummy # by nghttp2
fi
add_need libtasn1-6 # by gnutls28
add_need libtextwrap # by cdebconf
add_need libunistring # by gnutls28
add_need libxcrypt # by cyrus-sasl2, pam, shadow, systemd
add_need libxrender # by cairo
add_need libzstd # by systemd
add_need linux-atm # by iproute2
add_need lz4 # by systemd
add_need m4 # for debbuild
add_need make-dfsg # for build-essential
add_need man-db # for debhelper
add_need mawk # for base-files (alternatively: gawk)
add_need mpclib3 # by gcc-VER
add_need mpfr4 # by gcc-VER
add_need nettle # by unbound, gnutls28
add_need nss # by systemtap
add_need openssl # by cyrus-sasl2
add_need p11-kit # by gnutls28
#add_need python3-defaults
add_need patch # for dpkg-dev
add_need pcre2 # by libselinux
add_need perl # by debconf
add_need popt # by newt
add_need reprepro
add_need slang2 # by cdebconf, newt
#add_need sphinx # by python3-defaults, must build "all"
add_need sqlite3 # by python3.X, systemtap
add_need systemtap # by python3.X
add_need tcl8.6 # by newt
add_need tcltk-defaults # by python3.X
add_need tcp-wrappers # by audit
add_need texinfo
add_need tzdata
add_need xz-utils # by libxml2

# Vortex:
add_need util-linux
patch_util_linux() {
	if [ "$HOST_ARCH" = "i586" ]; then
		drop_privs patch -p1 <<'EOF'
--- util-linux-2.38.1/sys-utils/setarch.c
+++ util-linux-2.38.1/sys-utils/setarch.c
@@ -157,7 +157,11 @@
 #if defined(__x86_64__) || defined(__i386__) || defined(__ia64__)
 		{PER_LINUX32,	"i386",		"i386"},
 		{PER_LINUX32,	"i486",		"i386"},
+#if defined(__i586__)
+		{PER_LINUX32,	"i586",		"i586"},
+#else
 		{PER_LINUX32,	"i586",		"i386"},
+#endif
 		{PER_LINUX32,	"i686",		"i386"},
 		{PER_LINUX32,	"athlon",	"i386"},
 #endif
EOF
	fi
}

add_need gdisk
add_need syslinux
add_need busybox

# Fix internal error in lto
patch_systemd() {
	# lto-wrapper failed with
	# lto1: internal compiler error: original not compressed with zstd
	drop_privs patch -p1 <<'EOF'
--- a/debian/rules
+++ a/debian/rules
@@ -31,7 +31,7 @@
 endif
 
 CONFFLAGS = \
-	-Db_lto=true \
+	-Db_lto=false \
 	-Db_pie=true \
 	-Dmode=release \
 	-Drootlibdir=/usr/lib/$(DEB_HOST_MULTIARCH) \
EOF
}

automatically_cross_build_packages() {
	local dosetmp profiles buildable new_needed line pkg missing source
	while test -n "$need_packages"; do
		echo "checking packages with dose-builddebcheck: $need_packages"
		dosetmp=`mktemp -t doseoutput.XXXXXXXXXX`
		profiles="$DEFAULT_PROFILES"
		if test "$ENABLE_MULTILIB" = no; then
			profiles=$(set_add "$profiles" nobiarch)
		fi
		call_dose_builddebcheck --successes --failures --explain --latest=1 --deb-drop-b-d-indep "--deb-profiles=$(join_words , $profiles)" "--checkonly=$(join_words , $need_packages)" >"$dosetmp"
		buildable=
		new_needed=
		while IFS= read -r line; do
			case "$line" in
				"  package: "*)
					pkg=${line#  package: }
					pkg=${pkg#src:} # dose3 << 4.1
				;;
				"  status: ok")
					buildable=`set_add "$buildable" "$pkg"`
				;;
				"      unsat-dependency: "*)
					missing=${line#*: }
					missing=${missing%% | *} # drop alternatives
					missing=${missing% (* *)} # drop version constraint
					missing=${missing%:$HOST_ARCH} # skip architecture
					if is_arch_all "$missing"; then
						echo "rebootstrap-warning: $pkg misses dependency $missing which is arch:all"
					else
						source=`what_builds "$missing"`
						case "$source" in
							"")
								echo "rebootstrap-warning: $pkg transitively build-depends on $missing, but no source package could be determined"
							;;
							*" "*)
								echo "rebootstrap-warning: $pkg transitively build-depends on $missing, but it is build from multiple source packages: $source"
							;;
							*)
								if set_contains "$built_packages" "$source"; then
									echo "rebootstrap-warning: $pkg transitively build-depends on $missing, which is built from $source, which is supposedly already built"
								elif set_contains "$need_packages" "$source"; then
									echo "rebootstrap-debug: $pkg transitively build-depends on $missing, which is built from $source and already scheduled for building"
								elif set_contains "$automatic_packages" "$source"; then
									new_needed=`set_add "$new_needed" "$source"`
								else
									echo "rebootstrap-warning: $pkg transitively build-depends on $missing, which is built from $source but not automatic"
								fi
							;;
						esac
					fi
				;;
			esac
		done < "$dosetmp"
		rm "$dosetmp"
		echo "buildable packages: $buildable"
		echo "new packages needed: $new_needed"
		test -z "$buildable" -a -z "$new_needed" && break
		for pkg in $buildable; do
			echo "cross building $pkg"
			cross_build "$pkg"
			mark_built "$pkg"
		done
		need_packages=`set_union "$need_packages" "$new_needed"`
	done
	echo "done automatically cross building packages. left: $need_packages"
}

assert_built() {
	local missing_pkgs profiles
	missing_pkgs=`set_difference "$1" "$built_packages"`
	test -z "$missing_pkgs" && return 0
	echo "rebootstrap-error: missing asserted packages: $missing_pkgs"
	missing_pkgs=`set_union "$missing_pkgs" "$need_packages"`
	profiles="$DEFAULT_PROFILES"
	if test "$ENABLE_MULTILIB" = no; then
		profiles=$(set_add "$profiles" nobiarch)
	fi
	call_dose_builddebcheck --failures --explain --latest=1 --deb-drop-b-d-indep "--deb-profiles=$(join_words , $profiles)" "--checkonly=$(join_words , $missing_pkgs)"
	return 1
}

automatically_cross_build_packages

cross_build zlib "$(if test "$ENABLE_MULTILIB" != yes; then echo stage1; fi)"
mark_built zlib
# needed by dpkg, file, gnutls28, libpng1.6, libtool, libxml2, perl, slang2, tcl8.6, util-linux

automatically_cross_build_packages

cross_build libtool
mark_built libtool
# needed by guile-X.Y, libffi

automatically_cross_build_packages

cross_build ncurses
mark_built ncurses
# needed by bash, bsdmainutils, dpkg, guile-X.Y, readline, slang2

automatically_cross_build_packages

cross_build readline
mark_built readline
# needed by gnupg2, guile-X.Y, libxml2

automatically_cross_build_packages

if dpkg-architecture "-a$HOST_ARCH" -ilinux-any; then
	assert_built "libsepol pcre2"
	cross_build libselinux "nopython noruby" libselinux_1
	mark_built libselinux
# needed by coreutils, dpkg, findutils, glibc, sed, tar, util-linux

automatically_cross_build_packages
fi # $HOST_ARCH matches linux-any

dpkg-architecture "-a$HOST_ARCH" -ilinux-any && assert_built libselinux
assert_built "ncurses zlib"
cross_build util-linux "stage1 pkg.util-linux.noverity" util-linux_1
mark_built util-linux
# essential, needed by e2fsprogs

automatically_cross_build_packages

cross_build db5.3 "pkg.db5.3.notcl nojava" db5.3_1
mark_built db5.3
# needed by perl, python3.X, needed for db-defaults

automatically_cross_build_packages

cross_build libxml2 nopython libxml2_1
mark_built libxml2
# needed by nghttp2

automatically_cross_build_packages

cross_build cracklib2 nopython cracklib2_1
mark_built cracklib2
# needed by pam

automatically_cross_build_packages

cross_build build-essential cross build-essential_1 "any,all"
mark_built build-essential
# build-essential

automatically_cross_build_packages

cross_build pam stage1 pam_1 "any,all"
mark_built pam
# needed by shadow

automatically_cross_build_packages

assert_built "db-defaults db5.3 pam sqlite3 openssl"
cross_build cyrus-sasl2 "pkg.cyrus-sasl2.nogssapi pkg.cyrus-sasl2.noldap pkg.cyrus-sasl2.nosql" cyrus-sask2_1
mark_built cyrus-sasl2
# needed by openldap

automatically_cross_build_packages

assert_built "libevent expat nettle"
dpkg-architecture "-a$HOST_ARCH" -ilinux-any || assert_built libbsd
cross_build unbound pkg.unbound.libonly unbound_1
mark_built unbound
# needed by gnutls28

automatically_cross_build_packages

assert_built "gmp libidn2 p11-kit libtasn1-6 unbound libunistring nettle"
cross_build gnutls28 noguile gnutls28_1
mark_built gnutls28
# needed by libprelude, openldap, curl

automatically_cross_build_packages

assert_built "gnutls28 cyrus-sasl2"
cross_build openldap pkg.openldap.noslapd openldap_1
mark_built openldap
# needed by curl

automatically_cross_build_packages

if apt-cache showsrc systemd | grep -q "^Build-Depends:.*gnu-efi[^,]*[[ ]$HOST_ARCH[] ]"; then
cross_build gnu-efi
mark_built gnu-efi
# needed by systemd

automatically_cross_build_packages
fi

if test "$(dpkg-architecture "-a$HOST_ARCH" -qDEB_HOST_ARCH_OS)" = linux; then
if apt-cache showsrc man-db systemd | grep -q "^Build-Depends:.*libseccomp-dev[^,]*[[ ]$HOST_ARCH[] ]"; then
	cross_build libseccomp nopython libseccomp_1
	mark_built libseccomp
	# needed by man-db, systemd

	automatically_cross_build_packages
fi


assert_built "libcap2 pam libselinux acl xz-utils libgcrypt20 kmod util-linux libzstd"
if apt-cache showsrc systemd | grep -q "^Build-Depends:.*libseccomp-dev[^,]*[[ ]$HOST_ARCH[] ]" debian/control; then
	assert_built libseccomp
fi
cross_build systemd stage1 systemd_1
mark_built systemd
# needed by util-linux

automatically_cross_build_packages

assert_built attr
cross_build libcap-ng nopython libcap-ng_1
mark_built libcap-ng
# needed by audit

automatically_cross_build_packages

assert_built "gnutls28 libgcrypt20 libtool"
cross_build libprelude "nolua noperl nopython noruby" libprelude_1
mark_built libprelude
# needed by audit

automatically_cross_build_packages

assert_built "zlib bzip2 xz-utils"
cross_build elfutils pkg.elfutils.nodebuginfod
mark_built elfutils
# needed by glib2.0, systemtap

automatically_cross_build_packages

assert_built "libcap-ng krb5 openldap libprelude tcp-wrappers"
cross_build audit nopython audit_1
mark_built audit
# needed by libsemanage

automatically_cross_build_packages

assert_built "audit bzip2 libselinux libsepol"
cross_build libsemanage "nocheck nopython noruby" libsemanage_1
mark_built libsemanage
# needed by shadow

automatically_cross_build_packages
fi # $HOST_ARCH matches linux-any

dpkg-architecture "-a$HOST_ARCH" -ilinux-any && assert_built "audit libcap-ng libselinux systemd"
assert_built "ncurses zlib"
cross_build util-linux "pkg.util-linux.noverity"
# essential

automatically_cross_build_packages

cross_build brotli nopython brotli_1
mark_built brotli
# needed by curl

automatically_cross_build_packages

cross_build gdbm pkg.gdbm.nodietlibc gdbm_1
mark_built gdbm
# needed by man-db, perl, python3.X

automatically_cross_build_packages

patch_newt() {
	# fixing newt: needs docbook #1033417
	apt_get_remove docbook-xml
	# Respect nopython #1033465
	drop_privs patch -p1 <<'EOF'
--- a/debian/rules
+++ a/debian/rules
@@ -16,7 +16,11 @@
 DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
 LIBDIR:=/usr/lib/$(DEB_HOST_MULTIARCH)
 DEB_HOST_ARCH_OS  ?= $(shell dpkg-architecture -qDEB_HOST_ARCH_OS)
+ifeq ($(filter nopython,$(DEB_BUILD_PROFILES)),)
 PY3VERS:=$(shell py3versions --supported)
+else
+PYTHONSUPPORT= --without-python
+endif
 
 AM_VERS:=$(strip $(shell dpkg-query -f '$${source:Upstream-Version}' -W automake | egrep -o '^[0-9]+\.[0-9]+'))
 
@@ -45,6 +50,7 @@
 	# Nasty hack. why is it necessary?
 	cp /usr/share/automake-$(AM_VERS)/install-sh ./install-sh
 	dh_auto_configure  -- $(GPMSUPPORT) $(NOSTRIP) $(COLORSFILE) CFLAGS='-I/usr/include/tcl8.6 $(CFLAGS) -DMARCH=\"$(DEB_HOST_MULTIARCH)\" '  \
+		$(PYTHONSUPPORT) \
 		CPPFLAGS="$(CPPFLAGS)" LDFLAGS="$(LDFLAGS)" FFLAGS="$(FFLAGS)"  WHIPTCLLIB=whiptcl
 
 override_dh_auto_build:
--- a/Makefile.in
+++ b/Makefile.in
@@ -13,8 +13,10 @@
 SONAME = @SONAME@
 SOEXT = so
 
+ifneq ($(PYTHONVERS),)
 PYTHONVERS = $(shell py3versions --supported)
 PYTHONDBG := $(addsuffix -dbg, $(PYTHONVERS))
+endif
 WHIPTCLLIB = @WHIPTCLLIB@
 ifneq ($(WHIPTCLLIB),)
 WHIPTCLSO = $(WHIPTCLLIB).$(SOEXT)
EOF
}

cross_build newt nopython newt_1
mark_built newt
# needed by cdebconf

automatically_cross_build_packages

cross_build cdebconf pkg.cdebconf.nogtk cdebconf_1
mark_built cdebconf
# needed by base-passwd

automatically_cross_build_packages

# Problem with all,any ...
if [ "$BUILD_TYPE" != "any,all" ]; then
# Vortex:
assert_built "util-linux"
cross_build syslinux
mark_built syslinux
# needed by Vortex

automatically_cross_build_packages

assert_built "busybox syslinux"
fi # ! any,all

if test -f "$REPODIR/stamps/binutils_2"; then
	echo "skipping cross rebuild of binutils"
else
	cross_build_setup binutils binutils_2
	apt_get_build_dep "-a$HOST_ARCH" --arch-only -P nocheck ./
	check_binNMU
	# create binutils-i586-linux-gnu
	DEB_BUILD_OPTIONS="$DEB_BUILD_OPTIONS nocross nomult stage2" drop_privs dpkg-buildpackage "-a$HOST_ARCH" -Pnocheck -B -uc -us --target=stamps/control
	# stage2, because new arch is falling into "cross"
	DEB_BUILD_OPTIONS="$DEB_BUILD_OPTIONS nocross nomult stage2" drop_privs dpkg-buildpackage "-a$HOST_ARCH" -Pnocheck -B -uc -us
	rm -Rf /tmp/nodebugedit
	cd ..
	ls -l
	drop_privs sed -i -e '/^ .* binutils-for-host_.*deb$/d' ./*.changes
	pickup_additional_packages *.changes
	touch "$REPODIR/stamps/binutils_2"
	compare_native ./*.deb
	cd ..
	drop_privs rm -Rf binutils_2
fi
progress_mark "cross build binutils"
mark_built binutils
# needed for build-essential

if test -f "$REPODIR/stamps/gcc_2"; then
	echo "skipping cross-cross rebuild of gcc stage2"
else
	cross_build_setup "gcc-$GCC_VER" gcc2
	apt_get_install debhelper gawk patchutils bison flex lsb-release quilt libtool $GCC_AUTOCONF zlib1g-dev libmpc-dev libmpfr-dev libgmp-dev dejagnu systemtap-sdt-dev sharutils "binutils$HOST_ARCH_SUFFIX" time
	# configure: error: Building GCC requires GMP 4.2+, MPFR 3.1.0+ and MPC 0.8.0+.
	apt_get_install libmpc-dev:$HOST_ARCH libmpfr-dev:$HOST_ARCH libgmp-dev:$HOST_ARCH
	# /usr/lib/gcc/i586-linux-gnu/12/../../../../i586-linux-gnu/bin/ld: cannot find -lz: No such file or directory
	apt_get_install zlib1g-dev:$HOST_ARCH
	# libisl-dev:i586 not exist
	apt_get_build_dep "-a$HOST_ARCH" --arch-only -P nocheck ./ || :
	check_binNMU

	# No cross!
	rm -f debian/target
	(
		nolang=${GCC_NOLANG:-}
		test "$ENABLE_MULTILIB" = yes || nolang=$(set_add "$nolang" biarch)
		export DEB_BUILD_OPTIONS="$DEB_BUILD_OPTIONS${nolang:+ nolang=$(join_words , $nolang)}"
		if test "$ENABLE_MULTIARCH_GCC" = yes; then
			export with_deps_on_target_arch_pkgs=yes
		else
			export WITH_SYSROOT=/
		fi
		export gcc_cv_libc_provides_ssp=yes
		export gcc_cv_initfini_array=yes
		drop_privs dpkg-buildpackage -d "-a$HOST_ARCH" -Pnocheck -B -T control
		drop_privs dpkg-buildpackage -d "-a$HOST_ARCH" -Pnocheck -B -T clean
		dpkg-checkbuilddeps || : # tell unmet build depends again after rewriting control
		drop_privs dpkg-buildpackage -d "-a$HOST_ARCH" -Pnocheck -B -uc -us
	)

	cd ..
	ls -l
	#drop_privs sed -i -e '/^ .* gcc-for-host_.*deb$/d' ./*.changes
	# Remove duplicate with different checksums
	if test "$ENABLE_MULTIARCH_GCC" = yes; then
		drop_privs changestool ./*.changes dumbremove "gcc-${GCC_VER}-base_"*"_${HOST_ARCH}.deb"
		drop_privs rm "gcc-${GCC_VER}-base_"*"_${HOST_ARCH}.deb"

		drop_privs changestool ./*.changes dumbremove "gcc-${GCC_VER}-plugin-dev_"*"_${HOST_ARCH}.deb"
		drop_privs rm "gcc-${GCC_VER}-plugin-dev_"*"_$HOST_ARCH.deb"

		drop_privs changestool ./*.changes dumbremove "libatomic1_"*"_${HOST_ARCH}.deb"
		drop_privs rm "libatomic1_"*"_$HOST_ARCH.deb"

		# libgcc-12-dev_12.2.0-14_i586.deb libgcc-s1_12.2.0-14_i586.deb
		drop_privs changestool ./*.changes dumbremove "libgcc-"*"${GCC_VER}"*"_${HOST_ARCH}.deb"
		drop_privs rm "libgcc-"*"${GCC_VER}"*"_${HOST_ARCH}.deb"

		# libgomp1_12.2.0-14_i586.deb
		drop_privs changestool ./*.changes dumbremove "libgomp1_"*"_${HOST_ARCH}.deb"
		drop_privs rm "libgomp1_"*"_${HOST_ARCH}.deb"

		# libstdc++-12-dev_12.2.0-14_i586.deb, libstdc++-12-pic_12.2.0-14_i586.deb
		# libstdc++6-12-dbg_12.2.0-14_i586.deb
		# libstdc++6_12.2.0-14_i586.deb
		drop_privs changestool ./*.changes dumbremove "libstdc++"*"${GCC_VER}"*"_${HOST_ARCH}.deb"
		drop_privs rm "libstdc++"*"${GCC_VER}"*"_${HOST_ARCH}.deb"
	fi
	pickup_additional_packages *.changes
	touch "$REPODIR/stamps/gcc_2"
	compare_native ./*.deb
	cd ..
	drop_privs rm -Rf gcc2
fi
progress_mark "cross-cross build gcc stage2"
mark_built gcc_4
# needed for build-essential

# 07.06.2023
add_automatic gettext
# Profile "nodoc" has no effect, patch as workaround
patch_gettext() {
	# Cross can not run "../src/gettext --help" to create gettext.1
	# Disable all man 1 ...
	drop_privs patch -p1 <<'EOF'
--- gettext-0.21/gettext-runtime/man/Makefile.am
+++ gettext-0.21/gettext-runtime/man/Makefile.am
@@ -37,7 +37,7 @@
 man_MAN3IN = gettext.3.in ngettext.3.in \
 textdomain.3.in bindtextdomain.3.in bind_textdomain_codeset.3.in
 man_MAN3LINK = dgettext.3 dcgettext.3 dngettext.3 dcngettext.3
-man_MANS = $(man_MAN1)
+man_MANS = 
 notrans_man_MANS = $(man_MAN3) $(man_MAN3LINK)
 
 man_HTML1GEN = gettext.1.html ngettext.1.html
--- gettext-0.21/gettext-runtime/man/Makefile.in
+++ gettext-0.21/gettext-runtime/man/Makefile.in
@@ -1627,7 +1627,7 @@
 
 install-info-am:
 
-install-man: install-man1 install-man3
+install-man: install-man3
 
 install-pdf: install-pdf-am
 
--- gettext-0.21/gettext-tools/man/Makefile.am
+++ gettext-0.21/gettext-tools/man/Makefile.am
@@ -41,7 +41,7 @@
 man_MAN1MISC = \
 gettextize.1 autopoint.1
 man_MAN1 = $(man_MAN1SRC) $(man_MAN1MISC)
-man_MANS = $(man_MAN1)
+man_MANS = 
 
 man_HTML = \
 msgcmp.1.html msgfmt.1.html msgmerge.1.html msgunfmt.1.html xgettext.1.html \
@@ -50,7 +50,7 @@
 recode-sr-latin.1.html \
 gettextize.1.html autopoint.1.html
 
-EXTRA_DIST += help2man $(man_aux) $(man_MANS) $(man_HTML)
+EXTRA_DIST += help2man $(man_aux)
 MAINTAINERCLEANFILES = $(man_MANS) $(man_HTML)
 
 PERL = @PERL@
@@ -84,7 +84,7 @@
 #   make html
 
 all-local: html-local
-install-data-local: install-html
+install-data-local: 
 installdirs-local: installdirs-html
 uninstall-local: uninstall-html
 
EOF
}

cross_build gettext "nodoc nojava"
mark_built gettext
# need for debbuild

cross_build linux-atm
mark_built linux-atm

#automatically_cross_build_packages
cross_build iproute2
mark_built iproute2

cross_build init-system-helpers "cross" init_system_helpers_1 "any,all"
mark_built init_system_helpers_1

cross_build systemtap
mark_built systemtap

assert_built "systemtap"
cross_build python3.11 "cross" python3.11_1 "any,all"
mark_built python3.11_1

automatically_cross_build_packages
cross_build python3-defaults
mark_built python3-defaults
automatically_cross_build_packages

cross_build reprepro
mark_built reprepro
automatically_cross_build_packages

assert_built "$need_packages"

echo "checking installability of build-essential with dose"
apt_get_install botch
package_list=$(mktemp -t packages.XXXXXXXXXX)
grep-dctrl --exact --field Architecture '(' "$HOST_ARCH" --or all ')' /var/lib/apt/lists/*_Packages > "$package_list"
botch-distcheck-more-problems "--deb-native-arch=$HOST_ARCH" --successes --failures --explain --checkonly "build-essential:$HOST_ARCH" "--bg=deb://$package_list" "--fg=deb://$package_list" || :

#BUILD_ARCH=$(dpkg-architecture -qDEB_BUILD_ARCH)
#grep-dctrl --exact --field Architecture '(' "$BUILD_ARCH" --or all ')' /var/lib/apt/lists/*_Packages > "$package_list"
#botch-distcheck-more-problems "--deb-native-arch=$BUILD_ARCH" --successes --failures --explain --checkonly "crossbuild-essential-$HOST_ARCH:$BUILD_ARCH" "--bg=deb://$package_list" "--fg=deb://$package_list" || :
rm -f "$package_list"

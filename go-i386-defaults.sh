#!/bin/sh

# Mirror of repository
MIRROR=http://ftp.de.debian.org/debian

# where to run the build
CHROOT_DIR="../debian-unstable-i386-chroot"

mmdebstrap --verbose --variant=apt --mode=root \
      --customize-hook='upload bootstrap.sh /bootstrap.sh' \
      --customize-hook='chroot "$1" sh /bootstrap.sh HOST_ARCH=i386;' \
      unstable $CHROOT_DIR $MIRROR
